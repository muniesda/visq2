var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require('fs');
var cors = require('cors');
var async = require('async');
var json2xls = require('json2xls');
var flatten = require('flat')

var MongoClient = require('mongodb').MongoClient;

app.use(json2xls.middleware);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }))
//app.use(bodyParser.json());

app.get('/getstats', function(req, res){
  MongoClient.connect('mongodb://188.166.44.50:27017/visq', function(err, db) {
    var docsF = [];
    var writeStream = fs.createWriteStream("stats.xls");
    var smr = 'similarObject.relations.';
    var rs = 'relations.';

    var atts = ['time', 'user.age', 'user.field', 'user.gender', 'user.maps.thematic', 'user.maps.topographic', 'rating', 'similarObject.score', 'no_relations',
      'selectedFeature.typ', 'sizeCOefficient', 'selectedFeature.sizeCat', 'similarObject.sizeCat', 'angularityCOefficient', 'selectedFeature.angularityCat', 'similarObject.angularityCat',

      smr + '0.layerRate', smr + '0.score',  rs + '0.rank', smr + '0.type', rs + '0.surrounded', rs + '0.distance.0', rs + '0.distance.lvl', rs + '0.angle.0', rs + '0.angle.lvl', rs + '0.topology.1', rs + '0.topology.lvl',,
      smr + '0.group', smr + '0.distanceRate', smr + '0.distance', smr + '0.angleRate', smr + '0.angle', smr + '0.topology',smr + '0.topologyRate',

      smr + '1.layerRate', smr + '1.score',  rs + '1.rank', smr + '1.type', rs + '1.surrounded', rs + '1.distance.1', rs + '1.distance.lvl', rs + '1.angle.1', rs + '1.angle.lvl', rs + '1.topology.1', rs + '1.topology.lvl',,
      smr + '1.group', smr + '1.distanceRate', smr + '1.distance', smr + '1.angleRate', smr + '1.angle', smr + '1.topology',smr + '1.topologyRate',

      smr + '2.layerRate', smr + '2.score',  rs + '2.rank', smr + '2.type', rs + '2.surrounded', rs + '2.distance.2', rs + '2.distance.lvl', rs + '2.angle.2', rs + '2.angle.lvl', rs + '2.topology.1', rs + '2.topology.lvl',,
      smr + '2.group', smr + '2.distanceRate', smr + '2.distance', smr + '2.angleRate', smr + '2.angle', smr + '2.topology',smr + '2.topologyRate',

      smr + '3.layerRate', smr + '3.score',  rs + '3.rank', smr + '3.type', rs + '3.surrounded', rs + '3.distance.3', rs + '3.distance.lvl', rs + '3.angle.3', rs + '3.angle.lvl', rs + '3.topology.1', rs + '3.topology.lvl',,
      smr + '3.group', smr + '3.distanceRate', smr + '3.distance', smr + '3.angleRate', smr + '3.angle', smr + '3.topology',smr + '3.topologyRate',

      smr + '4.layerRate', smr + '4.score',  rs + '4.rank', smr + '4.type', rs + '4.surrounded', rs + '4.distance.4', rs + '4.distance.lvl', rs + '4.angle.4', rs + '4.angle.lvl', rs + '4.topology.1', rs + '4.topology.lvl',,
      smr + '4.group', smr + '4.distanceRate', smr + '4.distance', smr + '4.angleRate', smr + '4.angle', smr + '4.topology',smr + '4.topologyRate',

      smr + '5.layerRate', smr + '5.score',  rs + '5.rank', smr + '5.type', rs + '5.surrounded', rs + '5.distance.5', rs + '5.distance.lvl', rs + '5.angle.5', rs + '5.angle.lvl', rs + '5.topology.1', rs + '5.topology.lvl',,
      smr + '5.group', smr + '5.distanceRate', smr + '5.distance', smr + '5.angleRate', smr + '5.angle', smr + '5.topology',smr + '5.topologyRate',

      smr + '6.layerRate', smr + '6.score',  rs + '6.rank', smr + '6.type', rs + '6.surrounded', rs + '6.distance.6', rs + '6.distance.lvl', rs + '6.angle.6', rs + '6.angle.lvl', rs + '6.topology.1', rs + '6.topology.lvl',,
      smr + '6.group', smr + '6.distanceRate', smr + '6.distance', smr + '6.angleRate', smr + '6.angle', smr + '6.topology',smr + '6.topologyRate',

      smr + '7.layerRate', smr + '7.score',  rs + '7.rank', smr + '7.type', rs + '7.surrounded', rs + '7.distance.7', rs + '7.distance.lvl', rs + '7.angle.7', rs + '7.angle.lvl', rs + '7.topology.1', rs + '7.topology.lvl',,
      smr + '7.group', smr + '7.distanceRate', smr + '7.distance', smr + '7.angleRate', smr + '7.angle', smr + '7.topology',smr + '7.topologyRate',

      smr + '8.layerRate', smr + '8.score',  rs + '8.rank', smr + '8.type', rs + '8.surrounded', rs + '8.distance.8', rs + '8.distance.lvl', rs + '8.angle.8', rs + '8.angle.lvl', rs + '8.topology.1', rs + '8.topology.lvl',,
      smr + '8.group', smr + '8.distanceRate', smr + '8.distance', smr + '8.angleRate', smr + '8.angle', smr + '8.topology',smr + '8.topologyRate',

  ];

    var attsNames = ['time', 'u_age', 'u_field', 'u_gender', 'u_maps-them', 'user_maps-topo', 'user_rating', 'score', 'no_relations',
      'object_type', 'size_coefficient', 'selected_size_category', 'found_size_category', 'angularity_coefficient', 'selected_angularity_category', 'found_angularity_category',

      'r1-importance', 'r1-score', 'r1_selected-original_position', 'r1-typ', 'r1_selected-surrounded', 'r1_selected-dist_value', 'r1_selected-dist_gener', 'r1_selected-ang_value', 'r1_selected-ang_gener', 'r1_selected-top_value', 'r1_selected-top_gener',
      'r1_found-group', 'r1_found-dist_importance', 'r1_found-dist_score', 'r1_found-ang_importance', 'r1_found-ang_score', 'r1_found-top_score', 'r1_found-top_importance',

      'r2-importance', 'r2-score', 'r2_selected-original_position', 'r2-typ', 'r2_selected-surrounded', 'r2_selected-dist_value', 'r2_selected-dist_gener', 'r2_selected-ang_value', 'r2_selected-ang_gener', 'r2_selected-top_value', 'r2_selected-top_gener',
      'r2_found-group', 'r2_found-dist_importance', 'r2_found-dist_score', 'r2_found-ang_importance', 'r2_found-ang_score', 'r2_found-top_score', 'r2_found-top_importance',

      'r3-importance', 'r3-score', 'r3_selected-original_position', 'r3-typ', 'r3_selected-surrounded', 'r3_selected-dist_value', 'r3_selected-dist_gener', 'r3_selected-ang_value', 'r3_selected-ang_gener', 'r3_selected-top_value', 'r3_selected-top_gener',
      'r3_found-group', 'r3_found-dist_importance', 'r3_found-dist_score', 'r3_found-ang_importance', 'r3_found-ang_score', 'r3_found-top_score', 'r3_found-top_importance',

      'r4-importance', 'r4-score', 'r4_selected-original_position', 'r4-typ', 'r4_selected-surrounded', 'r4_selected-dist_value', 'r4_selected-dist_gener', 'r4_selected-ang_value', 'r4_selected-ang_gener', 'r4_selected-top_value', 'r4_selected-top_gener',
      'r4_found-group', 'r4_found-dist_importance', 'r4_found-dist_score', 'r4_found-ang_importance', 'r4_found-ang_score', 'r4_found-top_score', 'r4_found-top_importance',

      'r5-importance', 'r5-score', 'r5_selected-original_position', 'r5-typ', 'r5_selected-surrounded', 'r5_selected-dist_value', 'r5_selected-dist_gener', 'r5_selected-ang_value', 'r5_selected-ang_gener', 'r5_selected-top_value', 'r5_selected-top_gener',
      'r5_found-group', 'r5_found-dist_importance', 'r5_found-dist_score', 'r5_found-ang_importance', 'r5_found-ang_score', 'r5_found-top_score', 'r5_found-top_importance',

      'r6-importance', 'r6-score', 'r6_selected-original_position', 'r6-typ', 'r6_selected-surrounded', 'r6_selected-dist_value', 'r6_selected-dist_gener', 'r6_selected-ang_value', 'r6_selected-ang_gener', 'r6_selected-top_value', 'r6_selected-top_gener',
      'r6_found-group', 'r6_found-dist_importance', 'r6_found-dist_score', 'r6_found-ang_importance', 'r6_found-ang_score', 'r6_found-top_score', 'r6_found-top_importance',

      'r7-importance', 'r7-score', 'r7_selected-original_position', 'r7-typ', 'r7_selected-surrounded', 'r7_selected-dist_value', 'r7_selected-dist_gener', 'r7_selected-ang_value', 'r7_selected-ang_gener', 'r7_selected-top_value', 'r7_selected-top_gener',
      'r7_found-group', 'r7_found-dist_importance', 'r7_found-dist_score', 'r7_found-ang_importance', 'r7_found-ang_score', 'r7_found-top_score', 'r7_found-top_importance',

      'r8-importance', 'r8-score', 'r8_selected-original_position', 'r8-typ', 'r8_selected-surrounded', 'r8_selected-dist_value', 'r8_selected-dist_gener', 'r8_selected-ang_value', 'r8_selected-ang_gener', 'r8_selected-top_value', 'r8_selected-top_gener',
      'r8_found-group', 'r8_found-dist_importance', 'r8_found-dist_score', 'r8_found-ang_importance', 'r8_found-ang_score', 'r8_found-top_score', 'r8_found-top_importance',

      'r9-importance', 'r9-score', 'r9_selected-original_position', 'r9-typ', 'r9_selected-surrounded', 'r9_selected-dist_value', 'r9_selected-dist_gener', 'r9_selected-ang_value', 'r9_selected-ang_gener', 'r9_selected-top_value', 'r9_selected-top_gener',
      'r9_found-group', 'r9_found-dist_importance', 'r9_found-dist_score', 'r9_found-ang_importance', 'r9_found-ang_score', 'r9_found-top_score', 'r9_found-top_importance',
  ];


    writeStream.write(attsNames.join("\t") + "\n")

    db.collection('rating').find({}).toArray(function(err, docs){
      for (var di in docs){
        var docA = [];
        var fr = flatten(docs[di]);

        for (var ai in atts){
          if (fr[atts[ai]]){
            if (!isNaN(parseFloat(fr[atts[ai]]))){
              docA.push(parseFloat(fr[atts[ai]]).toPrecision(3));
            }else{
              docA.push(fr[atts[ai]]);
            }
          }else{
            docA.push('');
          }
        };

        if (docs[di]['relations']){
          docA[8] = docs[di]['relations'].length
        };


        //console.log(docA.join("\t") + "\n");
        writeStream.write(docA.join("\t") + "\n")
      };
      setTimeout(function(){
        writeStream.close();
        var file = __dirname + '/stats.xls';
        res.download(file);
      },2000)
    })
  })
});

app.post('/getData', function(req, res){
  //req = JSON.parse(req);
  fs.readFile(req.body.config, 'utf8', function (err2, config) {
    var config = JSON.parse(config);
    console.log(config);

    var layerNames = Object.keys(config);
    var readLayersNo = 0;
    var readLayers = {};

    async.map(layerNames, readAsync, function(err, geojsons) {
      var data = {
        "type": "FeatureCollection",
        "features": []
        };

        for (var gi in geojsons){
          var geojson = JSON.parse(geojsons[gi]);
          var layerName = layerNames[gi];

          for (var fi in geojson.features){
            var feature = geojson.features[fi];
            feature.typ = layerName;
            data.features.push(feature);
          };
        };
        res.send({geo: data, config: config}, 200);
      });
  });

  var readAsync = function(layerName, callback){
    fs.readFile('src/data/' + layerName + '.geojson', 'utf8', callback)
  };

});

app.post('/rate', function(req, res){
  var rateObj = JSON.parse(req.body.rate);


  MongoClient.connect('mongodb://188.166.44.50:27017/visq', function(err, db) {
    if(err) throw err;

    var ip = req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;

    console.log(rateObj);
    req.body.ip = ip;
    db.collection('rating').insert(rateObj, {w: 1}, function(){
      res.send('rating sent');
      console.log('rated');
    });
  });

});

app.listen(3000, function () {
  console.log('listening');
});
