var Data = function (data) {
  var that = this;

  //sorting features on their topology type
  var lines = [];
  var points = [];
  var polygons = [];

  _.forEach(data.features, function(feature, fi) {
    if (feature.geometry.type == 'LineString') {
      lines.push(feature);
    };
    if (feature.geometry.type == 'Point') {
      points.push(feature);
    };
    if (feature.geometry.type == 'Polygon') {
      polygons.push(feature);
    };
  });

  data.features = polygons.concat(lines, points)
  this.data = data;

  for (var fi in this.data.features) {
    var feature = this.data.features[fi];

    delete feature.properties.id
    feature['id'] = fi;

    feature['symbolic'] = {
      querySelected: false, // selected feature
      queryHighlighted: false, // mouseover effect
      queryActive: true, // feature that could be selected at the moment
      queryRelations: false, // features in active relation
      resultFound: false, // one of the results
      resultActive: false, // active result
      resultRelations: false, // features in relevant relation with active result
      resultHighlighted: false, // features in relation on mouseover
    };
  }

  _.mapKeys(layers, function(layer, layerName) {
    that.setAttributesForType(layerName);
  });
};

Data.prototype.refresh = function () {

};


// if ids is true, it will buffer all data
Data.prototype.bufferData = function (ids, buffer) {
  var bufferedData = [];

  if (ids != true){
    ids = _.map(ids, _.parseInt);
  };

  _.forEach(this.getFeatures(), function (feat) {
    if (_.includes(ids, parseInt(feat.id)) || ids == true){
      bufferedData.push(tm.buffer(feat, buffer, false));
    }
  });
  bufferedGeoJson = new L.geoJson(bufferedData);
  return bufferedGeoJson;
};

Data.prototype.getFeatures = function () {
  return this.data.features;
};

Data.prototype.turnAllFalse = function(typeOfFeatures) {
  _.forEach(this.getFeatures(), function(feat) {
      feat.symbolic[typeOfFeatures] = false;
  })
};

Data.prototype.setSymbolicMode = function(mode, featuresId, turnOff) {
  if (turnOff){
    this.turnAllFalse(mode);
  };

  _.forEach(this.getFeaturesById(featuresId), function(feat) {
    if(feat){
      feat.symbolic[mode] = true;
    };
  });
};

Data.prototype.refreshResultsFound = function(resultFoundFeatures) {
  resultFoundFeaturesIds = _.pluck(resultFoundFeatures, 'id');
  this.setSymbolicMode('resultFound', resultFoundFeaturesIds, true);
};

Data.prototype.getFeatureById = function(featureId) {
  if (parseInt(featureId) === featureId){
    featureId += '';
  }
  return _.find(this.getFeatures(), {id: featureId});
};

Data.prototype.getFeaturesById = function(featuresId) {
  features = [];
  for (var fii in featuresId){

    fid = featuresId[fii];
    features.push(this.getFeatureById(fid));
  }
  return features;
};

Data.prototype.getGhostedFeature = function() {
  return _.find(this.getFeatures(), {symbolic: {ghosted : true}});
};

Data.prototype.getActiveResultFeature = function() {
  return _.find(this.getFeatures(), {symbolic: {resultActive : true}});
};

Data.prototype.getSelectedFeature = function() {
  return _.find(this.getFeatures(), {symbolic: {querySelected : true}});
};

Data.prototype.getResultFeatures = function() {
  return _.filter(this.getFeatures(), {symbolic: {queryResult : true}});
};

Data.prototype.getFeaturesWithSelectedType = function() {
  return this.getFeaturesOfType(this.getSelectedFeature().typ);
};

Data.prototype.getFeaturesOfType = function(type) {
  return _.filter(this.getFeatures(), { 'typ': type });
};

Data.prototype.setAttributesForType = function(type) {

  var typeFs = this.getFeaturesOfType(type);
  var atts = typeFs[0].properties;

  _.mapKeys(atts, function(attVal, attKey) {
    atts[attKey] = {};
    atts[attKey]['values'] = [];
    atts[attKey]['name'] = attKey;
  });

  _.forEach(typeFs, function(f) {
    _.mapKeys(f.properties, function(value, key) {
      if (_.isString(value) || _.isNumber(value)){
        atts[key].values.push(value);
      }
    });
  });

  _.mapKeys(atts, function(attVal, attKey) {
    atts[attKey]['uniq'] = _.sortBy(_.uniq(atts[attKey].values, false));

    atts[attKey]['type'] = 'text';
    if (_.isNumber(atts[attKey]['uniq'][0])){
      if (atts[attKey]['uniq'].length > 5){
        atts[attKey]['type'] = 'number';
      }
    }
  });

  layers[type]['attributes'] = atts;
};
