var Controller = function(){
  map1.init();
  map2.init();
  map1.refreshData(data.data);
  map2.refreshData(data.data);
  selector.init(map1.map);
  queryBuffer.setMap(map1.map);
  resultBuffer.setMap(map2.map);
  queryHighlighter.setMap(map1.map);
  resultHighlighter.setMap(map2.map);
  panel.refresh();
};

Controller.prototype.prepareRateObject = function () {
  var rateObj = {
    "rating": results.rating,
    "similarObject": control.activeResult(),
    "relations": query.getActiveRelations(),
    "filter": query.selected.filter,
    "sizeCOefficient": query.size,
    "angularityCOefficient": query.angularity,
    "selectedFeature": control.getSelectedFeature(),
    "time": new Date(),
    "user": user
  };

  rateObj = JSON.stringify(rateObj);
  //rateObj = JSON.parse(rateObj.replace(/'/g, '"'));

  return rateObj;
};

// does a global refresh
Controller.prototype.refresh = function () {
  this.activateFeaturesInActiveRelations();
  data.refresh();
  this.refreshMaps();
  panel.refresh();
};

// refreshes the symbology of both maps
Controller.prototype.refreshMaps = function () {
  map1.refreshData(data.data);
  map2.refreshData(data.data);
}

// for the dynamical change of maps size
Controller.prototype.setMapSize = function () {
  var dividerLeft = $('#map-divider').position().left;

  $('#map1').css('width', dividerLeft);
  $('#map2').css('width', ($(window).width() - 300) - dividerLeft);

  // refreshes showed map tiles
  map1.map.invalidateSize();
  map2.map.invalidateSize();
};

// resets all features symbology
Controller.prototype.activetaAllQueryFeatures = function(){
  allFeatureIds = _.pluck(data.getFeatures(), 'id')
  data.setSymbolicMode('queryActive', allFeatureIds, false)
};

// activates the process of selecting feature on map
Controller.prototype.startSelectingFeature = function () {
  this.activetaAllQueryFeatures();
  this.refresh();
  this.clickSelect();
};

// selecting the feature on map with click
Controller.prototype.clickSelect = function () {
  var that = this;
  $('#map1').css('cursor', 'crosshair', 'important');
  $('path').css('cursor', 'crosshair', 'important');

  function ghostSelector(e){
    var overPoint = new L.circle([e.latlng.lat, e.latlng.lng], 5);
    var ghostedFeature = tm.mouseselectFeature(data.getFeatures(), overPoint, 3500)[0];

    if (ghostedFeature && data.getGhostedFeature()){
      if (data.getGhostedFeature().id != ghostedFeature.id){
        data.setSymbolicMode('ghosted', [ghostedFeature.id], true);
        map1.refreshData(data.data);
      }
    }else if(!data.getGhostedFeature() && ghostedFeature){
      data.setSymbolicMode('ghosted', [ghostedFeature.id], true);
      map1.refreshData(data.data);
    }else if (!ghostedFeature && data.getGhostedFeature()){
      data.setSymbolicMode('ghosted', [], true);
      map1.refreshData(data.data);
    }
    $('#map1').css('cursor', 'crosshair', 'important');
  };

  map1.map.on('mousemove', ghostSelector);

  setTimeout( function () {
    map1.map.once('click', function(e){
      map1.map.off('mousemove', ghostSelector)
      var clickedPoint = new L.circle([e.latlng.lat, e.latlng.lng], 5);
      var selectedFeature = tm.mouseselectFeature(data.getFeatures(), clickedPoint, 3500)[0];

      that.setFeatureAsSelected(selectedFeature);
      $('#map1').css('cursor', 'default');
      $('path').css('cursor', 'default', 'important');
    });

  }, 200)
};

// sets a new feature as selected
Controller.prototype.setFeatureAsSelected = function (feature) {
  data.turnAllFalse('queryActive');
  data.setSymbolicMode('querySelected', [feature.id], true);
  query.setSelected(feature);
  this.setInitialRelations(feature);

  //map1.map.invalidateSize();
  map1.map.fitBounds(data.bufferData([feature.id], 500).getBounds());
  //map1.map.invalidateSize();

  this.createRelationBuffer(feature, queryBuffer);
  this.setActiveResult();
  this.refresh();
  $('[data-toggle="tooltip"]').tooltip();
};

// does all the magic stuff and creates a object with all the information about relation to selected feature
Controller.prototype.makeRelations = function (featureToCheck) {
  var that = this;

  var inRadius = tm.findFeatureInRadius(data.getFeatures(), featureToCheck, layers[featureToCheck.typ].radius);
  var byType = _.groupBy(inRadius, 'typ');

  var relationObject = {
    open: false
  };

  var prepareRelationObject = function (group, features, typeName) {
    var distance = 0;
    var featureIds = [];
    var angle = tm.angle(featureToCheck, features);
    var surrounded = tm.isSurrounded(rays, features);

    if (group){
      featureIds = _.pluck(features, 'id');
      distance = tm.averageDistanceForGroup(features, featureToCheck)[0];
    }else{
      featureIds = [features[0].id];
      distance = tm.distance(features[0], featureToCheck);
    };

    var topoType = topoplogyHelper (group, features, featureToCheck);

    return _.merge({
      'group': group,
      'surrounded': surrounded,
      'angle': {
        0: angle,
        1: tm.generalizeAngle(angle, 1),
        2: tm.generalizeAngle(angle, 2),
        3: tm.generalizeAngle(angle, 3),
        lvl: 1,
        rate: 1
      },
      'distance': {
        0: distance,
        1: tm.generalizeDistance(distance, 1),
        2: tm.generalizeDistance(distance, 2),
        3: tm.generalizeDistance(distance, 3),
        lvl: 1,
        rate: 1
      },
      'topology':{
        0: 'topology',
        1: topoType['degree1'],
        2: topoType['degree2'],
        3: topoType['degree3'],
        sql: topoType['sql'],
        lvl: 1,
        rate: 1
      },
      'features': featureIds,
      'type': typeName,
      'filter': that.createInitFilterForRelation(typeName)
    }, relationObject)
  };

  var relations = [];

  var rays = tm.createRays(featureToCheck, layers[featureToCheck.typ].radius);

  _.mapKeys(byType, function(features, typeName) {

    if (features.length > layers[typeName].groupLimit){
      var GroupRelation = prepareRelationObject(true, features, typeName);
      var closestRelation = prepareRelationObject(false, [features[0]], typeName);

      relations.push(GroupRelation);
      relations.push(closestRelation);
    }else{
      for (var fi in features){
        var newRelation = prepareRelationObject(false, [features[fi]], typeName);
        relations.push(newRelation);
      }
    }
  })

  return this.sortRelations(relations);
};

// welcome window
Controller.prototype.showWelcomeWindow = function () {
  var that = this;

  // welcome window
  var welcomeText = translate('select language') + ': <select id="welcome-language-select" >';
  welcomeText += '<option class="language-mode" value="cz">' + translate('czech') + '</option>';
  welcomeText += '<option class="language-mode" value="en">' + translate('english') + '</option>';
  welcomeText += '</select>';
  welcomeText += '</br>';
  welcomeText += '</br>';

  if (language == 'cz'){
    welcomeText += 'Vítejte v aplikaci pro testování konceptu vizuálního dotazování na mapě. Jejím účelem je zhodnotit rozdíl mezi automatickým a subjektivním hledáním podobností na mapě</br>';
    welcomeText += '<ul>';
    welcomeText += '<li>1. Začnete kliknutím na ikonu nahoře vlevo na levé mapě a výběrem objektu</li>';
    welcomeText += '<li>2. Na panelu "Filter" vlevo si nastavte relace, které se budou započítávat do kalkulace podobnosti</li>';
    welcomeText += '<li>3. Přepněte panel na "Výsledky" a použijte tlačítko "hledej podobné objekty" pro vyhledání podobných objektů k referenčnímu</li>';
    welcomeText += '<li>4. Tlačítka s šipkami fungují na přechod mezi výsledky, daný objekt se s platnými relacemi zobrazuje na pravé mapě</li>';
    welcomeText += '<li>5. Použijte prosím oblast "hodnocení" a ohodnoďte podobnost objektů na mapových oknech. Škála podobnosti je 1 - 5 hvězdiček. K potvrzení je třeba použít tlačítko "odeslat"</li>';
    welcomeText += '</ul>';
  }

  else if (language == 'en'){
    welcomeText += 'Welcome to the application for testing the concept of visual querying on map. Its purpose is to find differences between automatic and manual rating of similarity on maps</br>';
    welcomeText += '<ul>';
    welcomeText += '<li>1. Use the only button in upper-left part of the left map to select a initial map object</li>';
    welcomeText += '<li>2. The left panel "Filter" is for setting all relevant relations for this object</li>';
    welcomeText += '<li>3. Change this panel to mode "Results" and use the button "update results" to search all similar object to the reference one</li>';
    welcomeText += '<li>4. You can change the active results by using buttons with left and right arrow. Active result is also shown on map</li>';
    welcomeText += '<li>5. Please, use the "rating" and rate the similarity of these two object on scale 1 to 5. You also have to use the "send" button to save this rating</li>';
    welcomeText += '</ul>'
  }


  welcomeText += '</br>';
  control.showAlert('VISUAL QUERY TESTING APPLICATION', welcomeText);
  $('#welcome-language-select').val(language)
  $('#welcome-language-select').change(function(){
    language = $(this).val();
    that.hideAlert();
    $('.modal-backdrop').remove();
    that.refresh();
    questionnaire.refresh();
    that.showWelcomeWindow();
  })
}

Controller.prototype.findSimilarObjects = function () {
  var that = this;
  if (data.getSelectedFeature()){
    var attributes = layers[data.getSelectedFeature().typ].attributes;

    var selectedTypeFeatures = data.getFeaturesWithSelectedType();
    var selectedTypeFeaturesFiltered = query.selectedAttributeFilter(selectedTypeFeatures, attributes);

    if (selectedTypeFeaturesFiltered.length > 1){
      var selectedFeature = data.getSelectedFeature();
      var similarities = [];
      control.showAlert(translate('calculating'), 'calculation started');

      _.each (selectedTypeFeaturesFiltered, function (selectedTypeFeature, fid){
        setTimeout(function(){

          if (selectedFeature.id != selectedTypeFeature.id){
            var featureRelations = that.makeRelations(selectedTypeFeature);
            var similarityResult = query.compareWithRelations(featureRelations, selectedTypeFeature);
            similarityResult.id = selectedTypeFeature.id;

            if (fid % 5 == 0){ // as we dont need to refresh loading progress on every feature
              document.getElementById('alert-body').innerHTML = Base.toPercentual(fid/selectedTypeFeaturesFiltered.length);
            }
            //console.log(similarityResult.score);
            if (similarityResult.score > similarityScoreThrehshold){
              similarities.push(similarityResult);
            }
          }

          if (selectedTypeFeaturesFiltered.length == fid + 1){
            //console.log(sortedResults);
            var sortedResults = _.sortByOrder(similarities, 'score', ['desc']);
            if (sortedResults.length){
              //console.log(sortedResults);
              query.setResultFeatures(sortedResults);
              data.refreshResultsFound(sortedResults);
              that.setActiveResult(sortedResults[0].id);
              sqlPanel.change();
            }
            control.hideAlert();
          }
        }, 20 * fid)
      });
    }else{
      control.showAlert(translate('no geometry in results'), translate('all geometries were filtered out. Please change your filter'))
    };
  }else{
    control.showAlert(translate('nothing selected'), translate('select a geometry on the left map'))
  };
};

// a result with given featureId will be set as a new activeResult
Controller.prototype.setActiveResult = function (featureId) {
  data.setSymbolicMode('resultActive', [featureId], true);

  var resultFeature = query.getResultFeatureById(featureId);

  data.setSymbolicMode('resultRelations', [] , true);
  if (resultFeature){

    _.each(resultFeature.relations, function(relation){
      var relationIds = relation.featureIds;
      data.setSymbolicMode('resultRelations', relationIds , false);
    })

    this.refresh();
    this.createRelationBuffer(this.activeResult(), resultBuffer);

    results.rated = false;
    map2.fitExtent(resultBuffer.geometry);
  };
};

// a wrapper to data.getActiveResultFeature()
Controller.prototype.getActiveResultFeature = function () {
   return data.getActiveResultFeature()
};

// returns a result id of actually displayed result
Controller.prototype.indexOfActiveResult = function () {
  var activeResultFeature = this.getActiveResultFeature();
  if (activeResultFeature){
    var activeResultFeatureId = activeResultFeature.id;
    return query.indexOfResults(activeResultFeatureId);
  }else{
    return -1;
  }
};

// the next result will be set as the new result
Controller.prototype.nextActiveResult = function () {
  var activeResultFeatureId = this.getActiveResultFeature().id;

  var nextActive = query.nextActiveResult(activeResultFeatureId);
  if (nextActive){
    this.setActiveResult(nextActive.id);
  }
};

// the previous result will be set as the new result
Controller.prototype.previousActiveResult = function () {
  var activeResultFeatureId = this.getActiveResultFeature().id;

  var previousActive = query.previousActiveResult(activeResultFeatureId);
  if (previousActive){
    this.setActiveResult(previousActive.id);
  }
};

// not used - should be deleted
Controller.prototype.getResultFeatures = function () {
  dataResults = data.getResultFeatures();
  resultFeatures = [];

  _.forEach (dataResults, function(dataResult){
    resultFeatures.push(_.assign(query.getResultFeatureById(dataResult.id), dataResult));
  });

  return _.sortByOrder(resultFeatures, 'score', ['desc']);
};

// get object with information of both actual result similarities and geometry of that feature
Controller.prototype.activeResult = function () {
  var activeFeature = this.getActiveResultFeature();
  if (activeFeature){
    activeFeatureId = activeFeature.id;
    return this.getResultFeature(activeFeatureId);
  }else{
    return null;
  }
};

// merge information about feature geometry, feature properties and feature relations
Controller.prototype.getResultFeature = function (id) {
  var feature = data.getFeaturesById([id])[0];
  var result = query.getResultFeatureById(id);

  if (feature && result){
    return _.assign(result, feature);
  }else{
    return null;
  };
};

Controller.prototype.getFeaturesById = function (ids) {
  return data.getFeaturesById(ids);
};

// finds possible relations for selected feature
Controller.prototype.setInitialRelations = function (selected) {
  query.reset();
  relations = this.makeRelations(selected);

  if (relations.length < query.numberOfRelations){
    query.numberOfRelations = relations.length;
  };

  _.forEach (relations, function(relation, ri){
    query.addRelation(relation);
  });
};

// sorts relations by distance, group value and type
Controller.prototype.sortRelations = function (relations) {
  // sort relations by distances
  var sortedByDistance = _.sortBy(relations, 'distance');

  // prefer individual relations
  var sortedByGroups = _.sortBy(relations, 'group');

  // send the second relation of the same on the end of array
  typesInside = [];
  var firstGroup = [];
  var secondGroup = [];

  _.forEach(sortedByGroups, function(relation, ri){
    if (_.indexOf(typesInside, relation.type) == -1){
      firstGroup.push(relation);
      typesInside.push(relation.type);
    }else{
      secondGroup.push(relation);
    }
  })

  typeSorted = firstGroup.concat(secondGroup);

  return typeSorted;
};

Controller.prototype.getFeatureIdsInRelation = function(relId){
  return query.findRelationById(relId).features;
};

Controller.prototype.getFeatureIdsInActiveRelations = function(){
  var that = this;

  var relations = query.getActiveRelations();
  var featureIds = [];

  _.forEach(relations, function(rel, ri){
    featureIds = featureIds.concat(that.getFeatureIdsInRelation(rel.id));
  });

  return featureIds;
};

Controller.prototype.activateFeaturesInActiveRelations = function(){
  data.setSymbolicMode('queryRelations', this.getFeatureIdsInActiveRelations(), true);
};

// returns a object as a initial emptz filter object
Controller.prototype.createInitFilterForRelation = function(type){
  var filter = {};

  _.mapKeys(layers[type].attributes, function(att, attName) {
    filter[attName] = {};
    filter[attName].values = [];
    filter[attName].open = false;
  });

  return filter;
};

Controller.prototype.createRelationBuffer = function(featureToBuffer, buffer){
  var bufferGeometry = tm.buffer(featureToBuffer, layers[featureToBuffer.typ].radius, true);
  buffer.setLatLngs(bufferGeometry.coordinates);
};

// activate the highlighter of chosen feature/s in relation to selected or result feature
// highlighter will be shown in map with given mode
Controller.prototype.showHighlighter = function (mode, relId) {
  if (mode == 'query'){
    var rel = query.findRelationById(relId);

    if (rel){
      var featuresIds = rel.features;
      data.setSymbolicMode('queryHighlighted', featuresIds, true);
      var bufferedData = data.bufferData(featuresIds, 50);
      queryHighlighter.changeBounds(bufferedData.getBounds());
    }

  }else if (mode == 'result'){
    if (this.activeResult().relations[relId].found){
      var featuresIds = this.activeResult().relations[relId].featureIds;
      data.setSymbolicMode('resultHighlighted', featuresIds, true);
      var bufferedData = data.bufferData(featuresIds, 50);
      resultHighlighter.changeBounds(bufferedData.getBounds());
    };
  };

  this.refreshMaps();
};

Controller.prototype.hideHighlighter = function (mode) {
  if (mode == 'query'){
    data.turnAllFalse('queryHighlighted');
    queryHighlighter.remove();
  }else if (mode == 'result'){
    data.turnAllFalse('resultHighlighted');
    resultHighlighter.remove();
  };

  this.refreshMaps();
};

Controller.prototype.changeLanguage = function (newLang) {
  if (language != newLang){
    language = newLang;
    this.refresh();
  };
};

Controller.prototype.changeSimilarityScoreThrehshold = function (newScoreThresh) {
  if (similarityScoreThrehshold != newScoreThresh){
    similarityScoreThrehshold = newScoreThresh;
    this.refresh();
  };
};

Controller.prototype.getFeatureFromRelation = function(relation) {
  if (relation.id == 'selected'){
    return control.getSelectedFeature();
  }else if (!relation.group){
    return control.getFeaturesById([relation.features[0]])[0];
  }else{
    return control.getFeaturesById([relation.features[0]])[0];
  };
};

Controller.prototype.activateInQuery = function () {
  data.turnAllFalse('highlighted');
  queryHighlighter.remove();

  map1.refreshData(data.data);
};

Controller.prototype.getSelectedFeature = function () {
  return data.getSelectedFeature();
};

Controller.prototype.changeSigmaMode = function (newSigmaMode) {
  query.sigmaMode = parseInt(newSigmaMode);
};

// shows alert div and changes the header and body of that alert
Controller.prototype.showAlert = function (headerText, bodyText) {
  $('#alert-header').html(headerText);
  $('#alert-body').html(bodyText);
  $('#alert-modal').modal();
};

// hides alert div
Controller.prototype.hideAlert = function () {
  $('#alert-modal').modal('hide');
};
