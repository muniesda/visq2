var dictionary = {
  'translate': function(term){
    if (layers[term]){
      return layers[term]['labels'][language]
    }
    if (language == 'en'){
      return term
    };
    if (this.dictionary[language][term]){
      return this.dictionary[language][term];
    }else{
      return term;
    }
  },
  'dictionary': {
    'cz': {

      // topology types:
      'active': 'aktivní',
      'connected': 'návaznost',
      'cross': 'průsek',
      'ending in': 'končící v',
      'going through': 'průchod',
      'identical': 'identita',
      'inside': 'uvnitř',
      'intersection': 'průnik',
      'joint(merged)': 'souběh',
      'passive': 'pasivní',
      'outside': 'venku',
      'touch': 'dotyk',
      'unrelated': 'mimoběh',
      'with interaction': 's interakcí',
      'without interaction': 'bez interakce',

      // questionnaire
      'Questionnaire': 'Uživatel',
      'rarely': 'zřídka',
      'sometimes': 'průběžně',
      'often': 'často',
      'field': 'obor/pracovní zaměření',
      'age': 'věk',
      'topographic maps use': 'práce s topografickými mapami',
      'thematic maps use': 'práce s tématickými mapami',
      'gender': 'pohlaví',
      'man': 'muž',
      'woman': 'žena',
      'geosciences - natural sciences path': 'geovědy - přírodovědný směr',
      'geosciences - social path': 'geovědy - humanitní směr',
      'geosciences - technical path': 'geovědy - technický směr',
      'technical': 'technické',
      'not technical': 'netechnické',
      'other': 'jiné',

      // filter panel
      'nothing selected': 'nebyl vybrán objekt',
      'relations': 'vazby',
      'selected object': 'vybraný objekt',
      'relation inactive': 'vazba je neaktivní',
      'click to filter out': 'klikni pro zapnutí filtru',
      'click to not filter out': 'klikni pro vypnutí filtru',
      'filter attribute values': 'filtruj hodnoty atributu',
      'value of this feature': 'hodnota objektu',

      // alerts
      'select a geometry on the left map': 'pomocí nástrojů na levé mapě vyberte geometrii',

      // results panel
      'distance coefficient': 'koeficient vzdálenosti',
      'angle coefficient': 'koeficient úhlu',
      'topology coefficient': 'koeficient topologie',
      'update results': 'hledej podobné objekty',
      'similarity': 'podobnost',
      'send': 'odeslat',
      'rating': 'hodnocení',
      'importance': 'význam',
      'importance of relation': 'význam vazby',
      'not found': 'nenalezeno',
      'total': 'celkem',
      'coefficient': 'koeficient',
      'size': 'velikost',
      'large': 'velká',
      'medium': 'střední',
      'small': 'malá',
      'angularity': 'tvar',
      'simple': 'jednoduchý',
      'complex': 'složitý',
      'rating saved': 'hodnocení uloženo',
      'thank you for your rating!': 'děkujeme za vaše hodnocení',
      'all geometries were filtered out. Please change your filter': 'neostala žádna geometrie, změnte prosim nastavení filtrů',

      // relations
      'quadrantE': 'čtvrtkruh - východ',
      'quadrantN': 'čtvrtkruh - sever',
      'quadrantS': 'čtvrtkruh - jih',
      'quadrantW': 'čtvrtkruh - západ',
      'semiN': 'půlkruh - sever',
      'semiS': 'půlkruh - jih',
      'distance': 'vzdálenost',
      'angle': 'úhel',
      'topology': 'topologie',

      // GUI
      'number of relation to work with': 'počet vazeb',
      'Filter': 'Filtr',
      'Results': 'Výsledky',
      'Info': 'Info',
      'step': 'krok',
      'More...': 'Více...',
      'settings': 'nastavení',

      // modal
      'Search': 'Vyhledávání',
      'Other': 'Další',
      'similarity score threhshold': 'práh koeficientu podobnosti',
      'language': 'jazyk',
      'select language': 'vyberte jazyk',
      'Save changes': 'Uložit změny',
      'Save and continue': 'Uložit a pokračovat',
      'sorting method': 'funkce třídění',
      'Close': 'Zavřít',
      'Options': 'Nastavení',

    }
  }
}
