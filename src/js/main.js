var language = 'cz'; // default language is Czech. The second option is "en", English

var ISIDENTICALMETERS = 5;
var ISCLUSTERMETERS = 50;
var DEFAULTRADIUSSIZE = 700;
var TOMETERS = 111319.9; // geographical degree to meters

var SURROUNDINGCOEFFICIENTINLINE = 0.5; // relative number of rays in line to say that feature is surrounded
var SURROUNDINGCOEFFICIENTTOTAL = 0.7; // total number of rays to say that feature is surrounded

var similarityScoreThrehshold = 0.2; // object with similarity less than similarityScoreThrehshold will not be presented

// levels of opacity
var OPACITY = {
  0: 0.04,
  1: 0.15,
  2: 0.3,
  3: 0.5,
  4: 0.7,
  5: 0.9,
  6: 1,
};

var user = {
  'age': '0-12',
  'field': 'geosciences - natural sciences path',
  'gender': 'w',
  'maps': {
    'thematic': 'rarely',
    'topographic': 'rarely'
  }
}

MAPWORKFREQUENCE = {
  1: 'rarely',
  2: 'sometimes',
  3: 'often'
}

var GLOBALSTYLE = {
  'fillOpacity': OPACITY[0],
  'opacity': OPACITY[0],
  'clickable': false,
  'weight': 1
};

var SIZECATEGORIES = {
  1: 'small',
  2: 'medium',
  3: 'large'
};

var ANGULARITYCATEGORIES = {
  1: 'simple',
  2: 'complex',
};

/*
  geoPath - path to geojson file
  configPath - path to layer values and information
  on callback returns loaded files of geographical data and config file
*/
var loadData = function(configPath, callback){
  dataObj = {
    config: 'src/data/' + configPath
  }

  $.ajax({
    url: 'http://188.166.44.50:3000/getData',
    data: dataObj,
    method: 'POST',
    success: function(data){
      callback(data);
    }
  })
};

// shortcut to dictionary.translate method
var translate = function(term){
  return dictionary.translate(term);
}

var processData = function(geo, config){
  _.mapKeys(config, function(layer, ck){
    layer.style = _.merge(layer.style, GLOBALSTYLE);
  });

  // calculating angularity and size for all features
  for (var f in geo.features){
    var feat = geo.features[f];
    feat.size = tm.size(feat);
    feat.angularity = tm.angularity(feat);
  };

  window.layers = config;
  window.data = new Data(geo);

  var layerNames = Object.keys(config);

  // getting avg, max and min values for angularity and size values
  for (var lni in layerNames){
    var layerName = layerNames[lni];
    var layerFeatures = data.getFeaturesOfType(layerName);

    layers[layerName].sizeMin = _.min(_.pluck(layerFeatures, 'size'));
    layers[layerName].sizeVar = _.max(_.pluck(layerFeatures, 'size')) - layers[layerName].sizeMin;
    layers[layerName].angularityMin = _.min(_.pluck(layerFeatures, 'angularity'));
    layers[layerName].angularityVar = _.max(_.pluck(layerFeatures, 'angularity')) - layers[layerName].angularityMin;
  };


  // size and angular categories/intervals
  for (var f in geo.features){
    var feat = geo.features[f];
    var type = feat.typ;

    var sizeFactor = (feat.size - layers[type].sizeMin) / layers[type].sizeVar;
    var angleFactor = (feat.angularity - layers[type].angularityMin) / layers[type].angularityVar;

    if (sizeFactor < 0.33) {
      feat.sizeCat = 1;
    } else if (sizeFactor < 0.66) {
      feat.sizeCat = 2;
    } else {
      feat.sizeCat = 3;
    };

    if (angleFactor < 0.5){
      feat.angularityCat = 1;
    } else {
      feat.angularityCat = 2;
    }
  };
}

var onLoad = function(){

  loadData('brno.json', function(response){
    processData(response.geo, response.config);
    window.map1 = new Map1('map1');
    window.map2 = new Map2('map2');
    window.modal = new Modal();
    window.selector = new Selector();
    window.info = new Info();
    window.query = new Query();
    window.legend = new Legend(query);
    window.results = new Results(query);
    window.panel = new Panel();
    window.queryBuffer = new Buffer();
    window.resultBuffer = new Buffer();
    window.queryHighlighter = new Highlighter();
    window.resultHighlighter = new Highlighter();
    window.control = new Controller();
    control.showWelcomeWindow();
    window.sqlPanel = new SqlPanel();
    window.questionnaire = new Questionnaire();

    // map windows size changing
    $('#map-divider').mousedown(function(e){
      var drag = $(this);

      var initialLeft = $(this).position().left;
      drag.addClass('dragging');

      var dragging = function(e){
        if (e.clientX > 0 && e.clientX < $(this).width() - 300){
          drag.css('left', e.clientX);
          window.getSelection().removeAllRanges();
        };
      };

      $(window).on('mousemove', dragging);

      $(window).on('mouseup', function(e){
        drag.removeClass('dragging');
        $(window).off('mousemove', dragging);
        control.setMapSize();
      });
    });
  })
}

// helper functions for handling relations between features
var relationsHelper = {
  getDistance: function (relation) {
    return this.toString(relation.distance[relation.distance.lvl], 'distance');
  },

  getAngle: function (relation) {
    return translate(this.toString(relation.angle[relation.angle.lvl], 'angle'));
  },

  getTopology: function (relation) {
    return translate(this.getTopologyFunction(relation));
  },

  getTopologyFunction: function (relation) {
    return relation.topology[relation.topology.lvl];
  },

  toString: function (value, mode){
    var outString = '';

    if (_.isString(value)){
      outString = value;
    }

    if (_.isArray(value)){
      outString = value.join(' - ');
      if (mode == 'distance'){
        outString += ' m';
      }
      if (mode == 'angle'){
        outString += ' °';
      }
    }

    return outString;
  }
};

// supplementary funcitons
var Base = {

  /*
    @param (float) value
    @returns (string)
    converts relative number to percentual value, rounds it and add a symbol of "%"
  */
  toPercentual: function (value) {
    return (value * 100).toFixed(1) + '% ';
  },

  /*
    @param (Array) values
    @returns (number)
  */
  mean: function (values) {
    return _.sum(values) / values.length;
  },

  /*
    calculates values according to gauss function
  */
  gauss: function (noRelsTotal, relNo, sigmaMode) {
      var sigmaModes = {
        1: 0.2,
        2: 0.4,
        3: 0.6
      };

      var cdf = function(x, dev, mean) {
	       return 0.5 * erfc(-(x - mean) / (dev * Math.sqrt(2)));
      };

      var erfc = function(x) {
        var z = Math.abs(x);
        var t = 1 / (1 + z / 2);
        var r = t * Math.exp(-z * z - 1.26551223 + t * (1.00002368 + t * (0.37409196 + t * (0.09678418 + t * (-0.18628806 + t * (0.27886807 + t * (-1.13520398 + t * (1.48851587 + t * (-0.82215223 + t * 0.17087277)))))))))
        return x >= 0 ? r : 2 - r;
      };

      var x = 1 - 1/(noRelsTotal + 1) * relNo;
      var sigma = sigmaModes[sigmaMode];
      var mean = 0.5;
      var y = cdf(x, sigma, mean);
      return y;
  }
}
