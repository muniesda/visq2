var Buffer = function Buffer (){
  this.latLngs = [[[0,0],[1,1]]];
  this.style = {
    fillColor: '#e74c3c',
    color: '#e74c3c', 
    opacity: 0.5,
    fillOpacity: 0.05,
    weight: 0.5,
  };
  this.geometry = new L.polygon(this.latLngs, this.style );
}

Buffer.prototype.setMap = function (map) {
  this.geometry.addTo(map);
};

Buffer.prototype.setLatLngs = function (newLls) {
  this.latLngs = newLls;
  this.refresh();
}

Buffer.prototype.refresh = function () {
  this.geometry.setLatLngs(this.latLngs);
  this.geometry.redraw();
}

Buffer.prototype.remove = function () {
  this.setLatLngs(this.nolatLngs)
  this.geometry.redraw();
}
