var Highlighter = function Highlighter (){
  this.active = false;
  this.noBounds = [[1,1],[1,1]];
  this.bounds = [[1,1],[1,1]];
  this.style = {color: '#e74c3c', weight: 1, fill: '#e74c3c', 'fillOpacity': 0.1};
  this.geometry = new L.rectangle(this.bounds, this.style);
}

Highlighter.prototype.setMap = function (map) {
  this.geometry.addTo(map);
};

Highlighter.prototype.refresh = function () {
  var that = this;
  this.geometry.setBounds(this.bounds);
  this.geometry.redraw();
  setTimeout(function(){
    that.geometry.bringToFront();
  }, 100)
};

Highlighter.prototype.changeBounds = function (bounds) {
  this.bounds = bounds;
  this.refresh();
};

Highlighter.prototype.remove = function () {
  this.changeBounds(this.noBounds)
};
