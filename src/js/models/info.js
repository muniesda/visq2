var Info = function (queryO) {
  this.html = '';
};

Info.prototype.createHtml = function () {
  var html = '<div class="panels"  id="info-panel">';
  html += '<p><i class="btn btn-default open-settings btn-xl glyphicon glyphicon-cog">  ' + translate('settings') + '</i></p>';
  html += '<p><i class="btn btn-default open-user btn-xl glyphicon glyphicon-user">  ' + translate('user setting') + '</i></p>';
  html += '</div>';

  return html;
};

Info.prototype.refresh = function () {
  panel.refresh()
  this.setListeners();
};

Info.prototype.setListeners = function () {
  $('.open-settings').click(function(){
    modal.toggleOpen();
  });
  $('.open-user').click(function(){
    questionnaire.toggleOpen();
  })
};
