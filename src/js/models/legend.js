var Legend = function (queryO) {
  this.html = '';
  this.query = queryO;
};

Legend.prototype.refresh = function () {
  panel.refresh();
  this.setListeners();
};

Legend.prototype.createHtml = function () {
  var selected = data.getSelectedFeature();

  var html = '<div class="panels" id="query-panel">';

  html += '</br>';

  if (selected) html += '<h4>' + translate('selected object') + ':</h4>';
  html += this.createSelectedPanel(selected);
  if (selected) html += '<h4>' + translate('relations') + ':</h4>';

  html += '<div id="panels-relations">';

  for (var rqi in this.query.relations){
    var relation = this.query.relations[rqi];
    html += this.createRelationPanel(relation);
  };

  html += '</div>';
  html += '</div>';
  return html;
};

Legend.prototype.createSelectedPanel = function (selected) {
  var html = '<h3>' + translate('nothing selected') + '</h3>';
  if (selected){
    html = '<div class="panel panel-primary" rel_id="selected">';

    //header
    html += '<div class="panel-heading legend-panel-heading">';
    html += '<span class="btn-group pull-left">';
    html += '<h4 class="panel-title">' + translate(selected.typ) + ' (id: ' + selected.id + ')</h4>';
    html += '</span>';
    html += '</div>';

    //body
    html += '<div class="panel-body">';

    var selectedFilter = query.selected.filter;
    var selectedAttributes = layers[selected.typ].attributes

    for (var ai in selectedAttributes){
      var attribute  = selectedAttributes[ai];

      html += '<div class="attribute-filter" attribute="' + attribute.name + '">';
      html += '<b>' + layers[selected.typ]['attributes-helper'][attribute.name][language] + '</b>';
      html += '<span class="btn-group pull-right">';
      html += this.attributeOpenButton(selectedFilter[attribute.name].open);
      html += '</span>';

      if (selectedFilter[attribute.name].open) {
        if (attribute.type == 'text') {
          html += this.makeTextAttributePanel(attribute, query.selected, 'selected');
        }else {
          html += this.makeNumberAttributePanel(attribute, query.selected, 'selected');
        }
      }
      html += '</div>';
    }

    html += '<p class="attribute-panel-number min">';
    html += '<div class="slider">';
    html += '</br>';
    html += '<h5>' + translate('number of relation to work with') + '</h5>';
    html += '</br>';
    html += '</br>';
    html += '<input class="number-of-relation-slider" type="range" min="1" max="' + _.min([this.query.relations.length, 8]) + '" value="' + this.query.numberOfRelations + '" />'
    var valueMargin = 5 + (this.query.numberOfRelations / _.min([this.query.relations.length, 8])) * 215
    html += '<output id="rangevalue" style="margin-left:' + valueMargin + 'px">' + this.query.numberOfRelations + '</output>';
    html += '</div>';
    html += '</p>';

    html += '</div>';

    html += '</div>';
  }
  return html
};


Legend.prototype.createRelationPanel = function (relation) {
  var relationNo = _.indexOf(this.query.relations, relation);
  var active = this.query.numberOfRelations > relationNo;

  if (active) {
    var layerRate = Base.gauss(query.numberOfRelations - 1, relationNo, query.sigmaMode);
    var html = '<div class="panel panel-success" rel_id="' + relation.id + '" data-toggle="tooltip" title="' + translate('importance of relation') + ': ' + Base.toPercentual(layerRate) + '">';
  }else{
    var html = '<div class="panel panel-default" rel_id="' + relation.id + '" data-toggle="tooltip" title="' + translate('relation inactive') + '">';
  };

  //header
  html += '<div class="panel-heading legend-panel-heading">';
  html += '<span class="btn-group pull-left">';
  html += '<h6 class="panel-title">';
  html += '<b>' + (relationNo + 1) + '.</b>';
  html += this.groupButton(relation);
  html += this.surroundedButton(relation);

  html += translate(relation.type) ;
  html += '</h6>';
  html += '</span>';
  html += '<span class="btn-group pull-right">';
  html += '<i class="btn btn-default moveRelationTop glyphicon glyphicon-arrow-up btn-xs"> </i>';
  html += '<i class="btn btn-default moveRelationUp glyphicon glyphicon-menu-up btn-xs"> </i>';
  html += '<i class="btn btn-default moveRelationDown glyphicon glyphicon-menu-down btn-xs"> </i>';
  html += this.openButton(relation.open);
  html += '</span>';
  html += '</div>';

  //body
  if (relation.open){
    html += '<div class="panel-body">';

    html += this.createRelationReference(relation);
    var attributes = layers[relation.type].attributes;

    for (var ai in attributes){
      html += this.createAttributePanel(attributes[ai], relation);
    }

  }else{
    html += '<div class="panel-body panel-closed">';
  }

  html += '</div>';
  html += '</div>';
  return html
};

Legend.prototype.createRelationReference = function (relation){
  var html = '';

  html += '<div class="slider">';
  html += '<h4>' + translate('distance') + '</h4>';
  html += '<b>' + relationsHelper.getDistance(relation) + '</b>';
  html += '<input class="relation-panel-distance" type="range" min="1" max="3" value="' + relation.distance.lvl + '" />';
  html += '<div class="relation-rate-slider-div">';
  html += translate('importance');
  html += ': ' + relation.distance.rate.toFixed(1);
  html += '<input class="relation-rate-slider relation-panel-distance-rate" type="range" min="0" max="1" value="' + relation.distance.rate + '" step="0.1" />';
  html += '</div>';
  html += '</div>';

  if (relation.surrounded){
    html += '<h4>' + translate('angle') + '</h4>';
    html += '<h5><b>-surrounded!</b></h5>';
    html += '<div class="relation-rate-slider-div">';
    html += translate('importance');
    html += ': ' + relation.angle.rate.toFixed(1);
    html += '<input class="relation-rate-slider relation-panel-angle-rate" type="range" min="0" max="1" value="' + relation.angle.rate + '" step="0.1" />';
    html += '</div>';
    html += '</br>';
  }else{
    html += '<div class="slider">';
    html += '<h4>' + translate('angle') + '</h4>';
    html += '<b>' + relationsHelper.getAngle(relation) + '</b>';
    html += '<input class="relation-panel-angle" type="range" min="1" max="3" value="' + relation.angle.lvl + '" />';

    html += '<div class="relation-rate-slider-div">';
    html += translate('importance');
    html += ': ' + relation.angle.rate.toFixed(1);
    html += '<input class="relation-rate-slider relation-panel-angle-rate" type="range" min="0" max="1" value="' + relation.angle.rate + '" step="0.1" />';
    html += '</div>';
    html += '</div>';
  }

  html += '<div class="slider">';
  html += '<h4>' + translate('topology') + '</h4>';
  html += '<b>' + relationsHelper.getTopology(relation) + '</b>';
  html += '<input class="relation-panel-topology" type="range" min="1" max="3" value="' + relation.topology.lvl + '" />';
  html += '<div class="relation-rate-slider-div">';
  html += translate('importance');
  html += ': ' + relation.topology.rate.toFixed(1);
  html += '<input class="relation-rate-slider relation-panel-topology-rate" type="range" min="0" max="1" value="' + relation.topology.rate + '" step="0.1" />';
  html += '</div>';
  html += '</div>';

  return html
};

Legend.prototype.createAttributePanel = function (attribute, relation){
  var html = '<div class="attribute-filter" attribute="' + attribute.name + '">';
  html += '<b>' + layers[relation.type]['attributes-helper'][attribute.name][language] + '</b>';
  html += '<span class="btn-group pull-right">';
  html += this.attributeOpenButton(relation.filter[attribute.name].open);
  html += '</span>';

  if (relation.filter[attribute.name].open) {
    if (attribute.type == 'text') {
      html += this.makeTextAttributePanel(attribute, relation, 'relation');
    }else {
      html += this.makeNumberAttributePanel(attribute, relation, 'relation');
    }
  };

  html += '</div>';
  return html;
};

Legend.prototype.makeNumberAttributePanel = function (attribute, relation, type){
  var that = this;
  var values = layers[relation.type].attributes[attribute.name].values;

  var format = {top: 10, right: 30, bottom: 30, left: 10};
  format.width = 250 - format.left - format.right;
  format.height = 150 - format.top - format.bottom;

  var step = relation.filter[attribute.name].values[2]|| 10;
  var steps = [2,5,10,15,20]

  var x = d3.scale.linear()
      .domain([0, Math.max.apply(null, values)])
      .range([0, format.width]);

  var hdata = d3.layout.histogram()
      .bins(x.ticks(step))
      (values);

  var maxV = _.ceil(_.max(values)/hdata[0].dx) * hdata[0].dx;
  var minV = hdata[0].x;

  var min = relation.filter[attribute.name].values[0] || minV;
  min = _.ceil(min/hdata[0].dx) * hdata[0].dx;

  var max = relation.filter[attribute.name].values[1] || maxV;
  max = _.ceil(max/hdata[0].dx) * hdata[0].dx;

  var y = d3.scale.linear()
      .domain([0, d3.max(hdata, function(d) { return d.y; })])
      .range([format.height, 0]);

  var html = '<div class="attribute-panel attribute-panel-number" type="' + type + '">';

  var minValueInput = hdata[0].x;
  var maxValueInput = _.ceil(maxV/hdata[0].dx) * hdata[0].dx;

  html += '<p class="attribute-panel-number min">';
  html += '<div class="slider">';
  html += '<h5>min  <b id="rangevalue">' + min + '</b></h5>';
  html += '<input class="attribute-panel-number min" type="range" min="' + minV + '" max="' + maxV + '" value="' + min + '" step="' + hdata[0].dx + '" />';
  html += '</div>';
  html += '</p>';

  html += '<p class="attribute-panel-number max">';
  html += '<div class="slider">';
  html += '<h5>max  <b id="rangevalue">' + max + '</b></h5>';
  html += '<input class="attribute-panel-number max" type="range" min="' + minV + '" max="' + maxV + '" value="' + max + '" step="' + hdata[0].dx + '" />';
  html += '</div>';
  html += '</p>';

  setTimeout(function(){
    that.appendHistogram(relation.id, attribute.name, format, x, y, hdata, min, max)
  }, 50);

  html += '<div class="histogram">';
  html += '</div>';

  html += '<h5>' + translate('step') + '</h5>';
  html += '<select class="attribute-panel-number step" value="' + step + '"/>';

  for (var si in steps){
    if (step == steps[si]){
      html += '<option value="' + steps[si] + '" selected>' + steps[si] + '</option>';
    }else{
      html += '<option value="' + steps[si] + '">' + steps[si] + '</option>';
    }
  }

  html += '</select>'
  html += '</div>'

  return html
}

Legend.prototype.appendHistogram = function (relId, attName, format, x, y, hdata, min, max){
  var thisRelation = query.findRelationById(relId);
  var thisFeature = control.getFeatureFromRelation(thisRelation);
  var thisAttValue = parseFloat(thisFeature.properties[attName]);
  console.log(thisAttValue);

  var attDiv = $('.panel[rel_id="' + relId + '"]' ).find('.attribute-filter[attribute="' + attName + '"]');

  if (relId == 'selected'){
    attDiv = $('.panel[rel_id="selected"]' ).find('.attribute-filter[attribute="' + attName + '"]');
  }

  var div = $(attDiv).find('.histogram');
  var formatCount = d3.format(",.0f");

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var svg = d3.select(div[0]).append("svg")
      .attr("width", format.width + format.left + format.right)
      .attr("height", format.height + format.top + format.bottom)
    .append("g")
      .attr("transform", "translate(" + format.left + "," + format.top + ")");

  var bar = svg.selectAll(".bar")
      .data(hdata)
    .enter().append("g")
      .attr("class", "bar")
      .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

    bar.append("rect")
        .attr("x", 1)
        .attr("fill", function(d){
          if (thisAttValue < parseFloat(d.x + d.dx) && thisAttValue >= d.x  ){
            return 'black'
          } else if (d.x + hdata[0].dx > min && d.x + hdata[0].dx <= max) {
            return 'red'
          } else {
            return 'white'
          }
        })
        .attr("width", x(hdata[0].dx) - 1)
        .attr("height", function(d) { return format.height - y(d.y); });

    bar.append("text")
        .attr("dy", ".75em")
        .attr("y", function(d){if (d.y < 5){ return -10} else{return 6}})
        .attr("x", x(hdata[0].dx) / 2)
        .attr("text-anchor", "middle")
        .text(function(d) {
          if (formatCount(d.y) != 0){
            return formatCount(d.y);
          }else{
            return '';
          }
        })

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + format.height + ")")
    .call(xAxis);
}

//
Legend.prototype.makeTextAttributePanel = function (attribute, relation, type){
  var html = '<div class="attribute-panel attribute-panel-text" type="' + type + '" >';
  html += '<i>' + translate('filter attribute values') + '</i>';


  var thisFeature = control.getFeatureFromRelation(relation);

  for (var ui in attribute.uniq){
    var uValue = attribute.uniq[ui] + "";
    var valueName = uValue;

    if (layers[relation.type]['attributes-helper'][attribute.name]['values']){
      if (layers[relation.type]['attributes-helper'][attribute.name]['values'][uValue]){
        valueName = layers[relation.type]['attributes-helper'][attribute.name]['values'][uValue][language];
      };
    };

    var featureAttValue = thisFeature.properties[attribute.name];

    if (_.indexOf(relation.filter[attribute.name].values, uValue) == -1){
      html += '<p class="attribute-panel-text-value filtered" attributevalue="' + uValue + '">';
      html += '<a href="#" class="btn btn-default textFilter off glyphicon glyphicon-unchecked btn-xs" data-toggle="tooltip" title="' + translate('click to filter out') + '"> </a>';
      html += valueName;
    }else{
      html += '<p class="attribute-panel-text-value allowed" attributevalue="' + uValue + '">';
      html += '<a href="#" class="btn btn-default textFilter on glyphicon glyphicon-ok btn-xs" data-toggle="tooltip" title="' + translate('click to not filter out') + '"> </a>';
      html += '<strike>  ' + valueName + '  </strike>';
    }
    if (featureAttValue == uValue){
      html += '<span href="#" class="glyphicon glyphicon-flag btn-xs" aria-hidden="true" data-toggle="tooltip" title="' + translate('click to not filter out') + '"> </span>';
    }
    html += '</p>';
  }
  html += '</div>'
  return html
}

Legend.prototype.setListeners = function (){
  var that = this;

  // level of generalization controls
  $('.relation-panel-topology').change(function(){
    var relId = $(this).closest('.panel').attr('rel_id');
    var rel = that.query.findRelationById(relId);
    rel.topology.lvl = $(this).val();
    panel.refresh();
  });

  $('.relation-panel-distance').change(function(){
    var relId = $(this).closest('.panel').attr('rel_id');
    var rel = that.query.findRelationById(relId);
    rel.distance.lvl = $(this).val();
    panel.refresh();
  });

  $('.relation-panel-angle').change(function(){
    var relId = $(this).closest('.panel').attr('rel_id');
    var rel = that.query.findRelationById(relId);
    rel.angle.lvl = $(this).val();
    panel.refresh();
  });

  //change number of selectedRelations
  $('.number-of-relation-slider').change(function(){
    that.query.numberOfRelations = $(this).val();
    control.refresh();
  });

  $('.panel').mouseenter(function(){
    var relId = $(this).attr('rel_id');
    if (relId != 'selected'){
      control.showHighlighter('query', relId);
    };
  });

  $('.panel').mouseleave(function(){
    control.hideHighlighter('query');
  });

  $('.openButton').click(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    that.toggleOpen(relId);
  });

  //moving relations up and down
  $('.moveRelationTop').click(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    that.query.moveRankTop(relId);
    control.refresh();
  });

  $('.moveRelationUp').click(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    that.query.moveRank(relId, -1);
    control.refresh();
  });

  $('.moveRelationDown').click(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    that.query.moveRank(relId, 1);
    control.refresh();
  });

  //attributesPanels
  $('.attributeOpenButton').click(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var attName = $(this.closest('.attribute-filter')).attr('attribute');
    that.toggleAttributeOpen(relId, attName);
  });

  $('.textFilter').click(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var attName = $(this.closest('.attribute-filter')).attr('attribute');
    var attValue = $(this.closest('.attribute-panel-text-value')).attr('attributevalue');
    var type = $(this.closest('.attribute-panel')).attr('type');

    that.toggleAttributeTextFilter(relId, attName, attValue, type);
  });

  $('.attribute-panel-number.min').change(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var attName = $(this.closest('.attribute-filter')).attr('attribute');
    var attValue = $(this).val();
    var type = $(this.closest('.attribute-panel')).attr('type');

    that.changeMinFilter(relId, attName, attValue, type);
  });

  $('.attribute-panel-number.max').change(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var attName = $(this.closest('.attribute-filter')).attr('attribute');
    var attValue = $(this).val();
    var type = $(this.closest('.attribute-panel')).attr('type');

    that.changeMaxFilter(relId, attName, attValue, type);
  });

  $('.attribute-panel-number.step').change(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var attName = $(this.closest('.attribute-filter')).attr('attribute');
    var attValue = $(this).val();

    that.changeStepFilter(relId, attName, attValue);
  });

  $('.relation-panel-angle-rate').change(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var rel = that.query.findRelationById(relId);
    rel.angle.rate = parseFloat($(this).val());
    control.refresh();
  });

  $('.relation-panel-distance-rate').change(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var rel = that.query.findRelationById(relId);
    rel.distance.rate = parseFloat($(this).val());
    control.refresh();
  });

  $('.relation-panel-topology-rate').change(function(){
    var relId = $(this.closest('.panel')).attr('rel_id');
    var rel = that.query.findRelationById(relId);
    rel.topology.rate = parseFloat($(this).val());
    control.refresh();
  });

};

Legend.prototype.attributeOpenButton = function (open){
  if (!open){
    return '<i class="btn btn-default attributeOpenButton glyphicon glyphicon-option-vertical btn-xs"> </i>';
  }else{
    return '<i class="btn btn-default attributeOpenButton glyphicon glyphicon-menu-hamburger btn-xs"> </i>';
  }
};

Legend.prototype.openButton = function (open){
  if (!open){
    return '<i class="btn btn-default openButton glyphicon glyphicon-option-vertical btn-xs"> </i>';
  }else{
    return '<i class="btn btn-default openButton glyphicon glyphicon-menu-hamburger btn-xs"> </i>';
  }
};

Legend.prototype.groupButton = function (relation){
  if (relation.group){
    return '<span class="glyphicon glyphicon-tags btn-xs"> </span>';
  }else{
    return '<span class="glyphicon glyphicon-tag btn-xs"> </span>';
  }
};

Legend.prototype.surroundedButton = function (relation){
  if (relation.surrounded){
    return '<span class="glyphicon glyphicon-record btn-xs"> </span>';
  }else{
    return ''
  }
}

Legend.prototype.toggleOpen = function (id){
  var rel = this.query.findRelationById(id);
  var newOpen = !rel.open;
  //query.setAllAttributesInFilterClosed();
  //query.setAllRelationClosed();
  rel.open = newOpen;
  panel.refresh();
};

Legend.prototype.changeStepFilter = function(relId, attName, attValue){
  var rel = this.query.findRelationById(relId);
  rel.filter[attName].values[2] = attValue;
  panel.refresh();
};

Legend.prototype.changeMaxFilter = function(relId, attName, attValue, type){
  var rel = this.query.findRelationById(relId);
  rel.filter[attName].values[1] = attValue;
  panel.refresh();
};

Legend.prototype.changeMinFilter = function(relId, attName, attValue, type){
  var rel = this.query.findRelationById(relId);
  rel.filter[attName].values[0] = attValue;
  panel.refresh();
};

Legend.prototype.toggleAttributeOpen = function(relId, attName){
  var rel = this.query.findRelationById(relId);
  var newOpen = !rel.filter[attName].open;
  //query.setAllAttributesInFilterClosed();
  rel.filter[attName].open = newOpen;
  panel.refresh();
};

Legend.prototype.toggleAttributeTextFilter = function(relId, attName, attValue, type){
  console.log(type);
  var rel = this.query.findRelationById(relId);
  var attValues = rel.filter[attName].values;
  var attValue = attValue + "";

  if (_.indexOf(attValues, attValue) == -1){
    attValues.push(attValue)
  }else{
    rel.filter[attName].values = _.without(attValues, attValue);
  };

  panel.refresh();
};
