var Map1 = function (id) {
  this.id = id;
  this.div = document.getElementById(id);
}

Map1.prototype.init = function () {
  var that = this;

  this.map = L.map(this.id, {
    center: [48.95, 16.72],
    zoom: 13,
    maxZoom: 17,
    minZoom: 12,
    zoomControl:false,
  });

  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    opacity: 0.05,
  }).addTo(this.map);

  this.mapSelector = new MapSelector(this);
  this.mapSelector.init();

  this.mapScale = new MapScale(this);
  this.mapScale.init();

  this.map.setMaxBounds(data.bufferData(true, 2000).getBounds());
};

Map1.prototype.removeGeoJson = function () {
  if (this.geojson){
    this.map.removeLayer(this.geojson);
  };
};

Map1.prototype.refreshData = function (newData) {
  var that = this;

  this.removeGeoJson();

  this.geojson = L.geoJson(newData, {
    style: function(feature){
      return that.styleFeature(feature);
    },
    pointToLayer: function (feature, latlng) {
      return L.circle(latlng, 25);
    }
  })
  this.geojson.addTo(this.map);
}

Map1.prototype.styleFeature = function (feature) {
  style = jQuery.extend({}, layers[feature.typ]['style']);

  if (feature.symbolic.ghosted){
    style['fillOpacity'] = OPACITY[4];
    style['fillColor'] = '#e74c3c';
    style['weight'] = 2;
    style['color'] = '#e74c3c';
  }
  if (feature.symbolic.querySelected){
    style['fillOpacity'] = OPACITY[5];
    style['opacity'] = OPACITY[5];
    style['weight'] = 4;
    style['color'] = '#e74c3c';
  }else if (feature.symbolic.queryHighlighted){
    style['fillOpacity'] = OPACITY[6];
    style['opacity'] = OPACITY[6];
    style['weight'] = 2;
  }else if (feature.symbolic.queryRelations){
    style['fillOpacity'] = OPACITY[3];
    style['opacity'] = OPACITY[3];
    style['weight'] = 2;
  }else if (feature.symbolic.queryActive){
    style['fillOpacity'] = OPACITY[2];
    style['opacity'] = OPACITY[2];
  }

  return style;
}
