var Map2 = function (id) {
  this.id = id;
  this.div = document.getElementById(id);
}

Map2.prototype.init = function () {
  this.map = L.map(this.id, {
    center: [48.95, 16.72],
    zoom: 13,
    maxZoom: 17,
    minZoom: 12,
    zoomControl:false,
  });

  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    opacity: 0.05,
  }).addTo(this.map);

  this.mapScale = new MapScale(this);
  this.mapScale.init();

  this.map.setMaxBounds(data.bufferData(true, 2000).getBounds());
};

Map2.prototype.removeGeoJson = function () {
  if (this.geojson){
    this.map.removeLayer(this.geojson);
  };
};

Map2.prototype.refreshData = function (newData) {
  var that = this;

  this.removeGeoJson();

  this.geojson = L.geoJson(newData, {
    style: function(feature){
      return that.styleFeature(feature)
    },
    pointToLayer: function (feature, latlng) {
      return L.circle(latlng, 25);
    }
  })
  this.geojson.addTo(this.map);
}

Map2.prototype.fitExtent = function (feature) {
  if (feature.getBounds()){
    this.map.fitBounds(feature.getBounds(), false);
  }else {
    var featurePath = L.geoJson(feature);
    this.map.fitBounds(featurePath.getBounds(), false);
  }
}

Map2.prototype.styleFeature = function (feature) {
  style = jQuery.extend({}, layers[feature.typ]['style']);

  if (feature.symbolic.resultActive){
    style['fillOpacity'] = OPACITY[5];
    style['opacity'] = OPACITY[5];
    style['weight'] = 2;
    style['color'] = '#e74c3c';
  }else if (feature.symbolic.resultHighlighted){
    style['fillOpacity'] = OPACITY[6];
    style['opacity'] = OPACITY[6];
    style['weight'] = 2;
  } else if(feature.symbolic.resultRelations){
    style['fillOpacity'] = OPACITY[3];
    style['opacity'] = OPACITY[3];
    style['weight'] = 2;
  }else if (feature.symbolic.resultFound){
    style['fillOpacity'] = OPACITY[1];
    style['opacity'] = OPACITY[1];
  }
  return style;
}
