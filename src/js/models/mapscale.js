var MapScale = function (map) {
	this.map = map;
}


MapScale.prototype.init = function () {
	var that = this;

	if (this.map.id == 'map1'){
		this.control = L.control ({position: 'topright'});
	}else{
		this.control = L.control ({position: 'topleft'});
	}

	this.control.onAdd = function () {
	  this._div = L.DomUtil.create('i', '.leaflet-control map-scale');
	  return this._div;
	};

	this.map.map.on('zoomend', function(){
		that.refresh()
	})

	this.control.addTo(this.map.map);
	this.refresh()
};

MapScale.prototype.refresh = function () {
	//var zoom = this.map.map.getZoom();
	//var scale = (256 * Math.pow(2,zoom)) / (1000 * 1.60934);
	var bounds = this.map.map.getBounds();

  var centerLat = bounds.getCenter().lat;
	var halfWorldMeters = 6378137 * Math.PI * Math.cos(centerLat * Math.PI / 180);
	//length of this arc from map left to map right
	var diffLat = bounds.getNorthEast().lng - bounds.getSouthWest().lng;
	var dist = halfWorldMeters * (diffLat) / 180;
	var mapY = this.map.map.getSize().x;

	var scale = (mapY/dist) * 200;
	$(this.map.div).find('.map-scale').width(scale)
}
