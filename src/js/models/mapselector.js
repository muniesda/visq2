var MapSelector = function (map) {
	this.map = map;
}


MapSelector.prototype.init = function () {
	var that = this;

	this.control = L.control ({position: 'topleft'});
	this.control.onAdd = function () {
	  this._div = L.DomUtil.create('i', '.leaflet-control leaflet-control-selector glyphicon glyphicon-screenshot');
	  return this._div;
	};

	this.control.addTo(this.map.map);
	$('.leaflet-control-selector').click(function () {
	  control.startSelectingFeature()
	});

};
