var Modal = function(){
  this.html = '';
  this.open = false;
  this.div = $('#modal');
  this.refresh();
}

Modal.prototype.refresh = function () {
  if (this.open){
    this.div[0].innerHTML = this.createHtml()
    this.setListeners();
    $('.modal-backdrop').remove();
    $('#settings').modal()
  }else{
    this.div[0].innerHTML = ''
    $('#settings').modal('hide')
  }
};

Modal.prototype.toggleOpen = function () {
  this.open = $('#settings .modal').hasClass('in')
  this.open = !this.open;
  this.refresh()
}

Modal.prototype.createHtml = function () {
  var html = '<div class="modal" id="settings">'
  html += '<div class="modal-dialog">'
  html += '<div class="modal-content">'
  html += '<div class="modal-header">'
  html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
  html += '<h4 class="modal-title">' + translate('Options') + '</h4>'
  html += '</div>'
  html += '<div class="modal-body">'

  html += '<h4>' + translate('Search') + '</h4>';

  //gauss settings
  html += translate('sorting method') + ': <select id="sigma-mode-select" value="' + query.sigmaMode + '">';
  html += '<option class="sigma-mode" value="1">gauss mode1</option>';
  html += '<option class="sigma-mode" value="2">gauss mode2</option>';
  html += '<option class="sigma-mode" value="3">gauss mode3</option>';
  html += '</select>';

  //similarityScoreThrehshold
  html += '<br/>';
  html += translate('similarity score threhshold') + ': <select id="similarity-coef-select" value="' + similarityScoreThrehshold + '">';
  html += '<option class="sigma-mode" value="0.1">10%</option>';
  html += '<option class="sigma-mode" value="0.2">20%</option>';
  html += '<option class="sigma-mode" value="0.3">20%</option>';
  html += '<option class="sigma-mode" value="0.5">50%</option>';
  html += '</select>';


  html += '<h4>' + translate('Other') + '</h4>';

  //language settings
  html += translate('language') + ': <select id="language-select" value="' + language + '">';
  html += '<option class="language-mode" value="cz">cz</option>';
  html += '<option class="language-mode" value="en">en</option>';
  html += '</select>';

  html += '</div>'
  html += '<div class="modal-footer">'
  html += '<button type="button" class="save-changes btn btn-primary">' + translate('Save changes') + '</button>'
  html += '</div>'
  html += '</div>'
  html += '</div>'
  html += '</div>'
  return html
};

Modal.prototype.setListeners = function () {
  var that = this;
  $('#sigma-mode-select').val(query.sigmaMode);
  $('#similarity-coef-select').val(similarityScoreThrehshold);
  $('#language-select').val(language);

  $('#settings .save-changes').click(function(){
    control.changeSigmaMode($('#sigma-mode-select').val());
    control.changeLanguage($('#language-select').val());
    control.changeSimilarityScoreThrehshold($('#similarity-coef-select').val());
    that.refresh();
  });
};
