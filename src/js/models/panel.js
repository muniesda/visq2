var activePanel = 'legend'

var Panel = function (queryO) {
  this.div = document.getElementById('panel');
  this.html = '';
  this.query = queryO;
  this.legend = legend;
  this.results = results;
  this.info = info;
};

Panel.prototype.refresh = function () {
  var html = '<ul class="nav nav-pills" role="tablist">';

  if (activePanel == 'legend'){
    scrollDiv = '#query-panel';
    html += '<li class="panel-navbar-li legend active"><a>' + translate('Filter') + '</a></li>';
    html += '<li class="panel-navbar-li results"><a>' + translate('Results') + '</a></li>';
    html += '<li class="panel-navbar-li info"><a>' + translate('More...') + '</a></li>';

  } else if (activePanel == 'results'){
    scrollDiv = '#result-panel';
    html += '<li class="panel-navbar-li legend"><a>' + translate('Filter') + '</a></li>';
    html += '<li class="panel-navbar-li results active"><a>' + translate('Results') + '</a></li>';
    html += '<li class="panel-navbar-li info"><a>' + translate('More...') + '</a></li>';

  } else if (activePanel == 'info'){
    scrollDiv = '#info-panel';
    html += '<li class="panel-navbar-li legend"><a>' + translate('Filter') + '</a></li>';
    html += '<li class="panel-navbar-li results"><a>' + translate('Results') + '</a></li>';
    html += '<li class="panel-navbar-li info active"><a>' + translate('More...') + '</a></li>';
  };

  html += '</ul>';
  var scrollTop = $(scrollDiv).scrollTop();
  html += this[activePanel].createHtml();
  this.html = html;
  this.draw();
  this.setListeners();

  $(scrollDiv).scrollTop(scrollTop);
}

Panel.prototype.setListeners = function () {
  var that = this;

  $('.panel-navbar-li.legend').click(function(){
    activePanel = 'legend';
    that.refresh();
  });

  $('.panel-navbar-li.info').click(function(){
    activePanel = 'info';
    that.refresh();
  });

  $('.panel-navbar-li.results').click(function(){
    activePanel = 'results';
    that.refresh();
  });
  $('[data-toggle="tooltip"]').tooltip();
}

Panel.prototype.draw = function () {
  this.div.innerHTML = this.html;
  this[activePanel].setListeners();
}
