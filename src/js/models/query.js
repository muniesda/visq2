var Query = function(){
  this.relations = [];
  this.selected = {};
  this.resultFeatures = [];
  this.size = 0;
  this.angularity = 0;

  this.numberOfRelations = 3;

  this.sigmaMode = 1;
};

Query.prototype.isFiltered = function (featureToCheck, filterObjectProperties, attributes){
  filtered = false;
  _.each(_.keys(filterObjectProperties), function(prop, pi){
    var filteredValues = filterObjectProperties[prop].values;

    if (filteredValues){
      if (attributes[prop].type == 'number'){
        if(filteredValues[0]){
          if (parseFloat(featureToCheck.properties[prop]) < parseFloat(filteredValues[0])){
            filtered = true;
          }
        };
        if (filteredValues[1]){
          if (parseFloat(featureToCheck.properties[prop]) >= parseFloat(filteredValues[1])){
            filtered = true;
          }
        }
      }else{
        for (var fvi in filteredValues){
          if (featureToCheck.properties[prop] == filteredValues[fvi]){
            filtered = true;
          }
        }
      }
    }
  })
  return filtered;
};

Query.prototype.selectedAttributeFilter = function (featuresToCheck, attributes){
  var that = this;
  var filteredFeatures = [];

  _.each(featuresToCheck, function(feat, fi){
    filtered = that.isFiltered(feat, that.selected['filter'], attributes);

    if (!filtered){
      filteredFeatures.push(feat);
    }
  });

  return filteredFeatures;
};

Query.prototype.addRelation = function (newRelation){
  this.relations.push(_.merge({rank: this.relations.length, id: this.relations.length}, newRelation));
};

Query.prototype.reset = function (){
  this.relations = [];
};

Query.prototype.setSelected = function(){
  this.selected['filter'] = control.createInitFilterForRelation(control.getSelectedFeature().typ);
  this.selected['type'] = control.getSelectedFeature().typ;
  this.selected['id'] = 'selected';
};

Query.prototype.findRelationById = function(rel_id){
  if (rel_id == 'selected'){
    return this.selected
  };
  return _.find(this.relations, {'id': parseInt(rel_id)});
};

Query.prototype.setAllAttributesInFilterClosed = function(){
  _.forEach(this.relations, function(relation){
    _.forEach(relation.filter, function(attribute){
      attribute.open = false;
    })
  })
};

Query.prototype.setAllRelationClosed = function(){
  _.forEach(this.relations, function(relation){
    relation.open = false;
  })
};

Query.prototype.getActiveRelations = function(){
  return this.relations.slice(0, this.numberOfRelations)
};

Query.prototype.moveRankTop = function(id){
  var rel = this.findRelationById(id);
  if (rel){
    var oldIndex = _.indexOf(this.relations, rel);

    this.relations.splice(oldIndex, 1);
    this.relations.splice(0, 0, rel);
  }
};

Query.prototype.moveRank = function(id, positionChange){
  var rel = this.findRelationById(id);
  if (rel){
    var oldIndex = _.indexOf(this.relations, rel);
    var newIndex = (oldIndex + positionChange);

    if (newIndex < 0){
      newIndex = 0
    }
    if (newIndex >= this.relations.length){
      newIndex = this.relations.length
    }

    this.relations.splice(oldIndex, 1);
    this.relations.splice(newIndex, 0, rel);
  }
};

Query.prototype.compareWithRelations = function(compareRelations, compareFeature){
  var relationsSimilarities = [];

  var that = this;
  var activeRelations = query.getActiveRelations();

  _.each (activeRelations, function (activeRelation, arid){
    var activeGroup = activeRelation.group;
    var activeType = activeRelation.type;

    relationSimilarity = {
      found: false,
      score: 0,
      type: activeType
    };

    for (var crid in compareRelations) {
      var compareRelation = compareRelations[crid];


      if (compareRelation.type == activeType && compareRelation.group == activeGroup){
        if (!that.isFiltered(data.getFeatureById(compareRelation.features[0]), activeRelation.filter, layers[compareRelation.type].attributes)){

          var angleLvl = activeRelation.angle.lvl;
          var angleRate = activeRelation.angle.rate;
          var angleToCompare = compareRelation.angle[angleLvl];
          var activeAngle = activeRelation.angle[angleLvl];
          var angleDifference = that.compareAngle(angleToCompare, activeAngle, angleLvl);

          var distanceLvl = activeRelation.distance.lvl;
          var distanceRate = activeRelation.distance.rate;
          var distanceToCompare = compareRelation.distance[distanceLvl];
          var activeDistance = activeRelation.distance[distanceLvl];
          var distanceDifference = that.compareDistance(distanceToCompare, activeDistance);

          var topologyLvl = activeRelation.topology.lvl;
          var topologyRate = activeRelation.topology.rate;
          var topologyToCompare = compareRelation.topology[topologyLvl];
          var activeTopology = activeRelation.topology[distanceLvl];
          var topologyDifference = that.compareTopology(topologyToCompare, activeTopology, topologyLvl);

          var rateNormCoef = (3/(distanceRate + angleRate + topologyRate))/3
          angleRate = (angleRate * rateNormCoef).toFixed(10);
          distanceRate = (distanceRate * rateNormCoef).toFixed(10);
          topologyRate = (topologyRate * rateNormCoef).toFixed(10);

          // if the object is surrounded by objects with specific type, angle-similarity is not calculated in that case
          if (activeRelation.surrounded ){
            if (compareRelation.surrounded){
              angleDifference = 1;
            }else{
              angleDifference = 0;
            }
          };

          var score = distanceRate * distanceDifference + angleRate * angleDifference + topologyRate * topologyDifference;

          var ids = [];
          _.each( _.values(compareRelation.features), function(featureId){
            ids.push(parseInt(featureId));
          });

          relationSimilarity = {
            'featureIds': ids,
            'comparedRelation': activeRelation,
            'type': compareRelation.type,
            'group': compareRelation.group,
            'score': score,
            'distance': distanceDifference,
            'angle': angleDifference,
            'topology': topologyDifference,
            'distanceRate': distanceRate,
            'angleRate': angleRate,
            'topologyRate': topologyRate,
            'found': true
          }
          //console.log(relationSimilarity);
          break;

        }else{
          //console.log(compareRelation);
        }

      }
    }
    relationsSimilarities.push(relationSimilarity);
  })

  var noRels = relationsSimilarities.length;

  var score = 0;
  var layerRateSum = 0;

  _.each(relationsSimilarities, function(relation, rid){
    var ridPlus = rid + 1;
    var layerRate = Base.gauss(noRels, ridPlus, that.sigmaMode);
    score += layerRate * relation.score;
    layerRateSum += layerRate;
    relation.layerRate = layerRate;
  });

  _.each(relationsSimilarities, function(relation, rid){
    relation.layerRate *= (noRels/layerRateSum)/noRels;
  });

  score = score/(noRels*0.5);

  var sizeRate = this.size;
  var angularRate = this.angularity;

  var relationRate = 1 - sizeRate - angularRate;

  var totalScore = score * relationRate;

  if (compareFeature.sizeCat == control.getSelectedFeature().sizeCat){
    totalScore += parseFloat(sizeRate);
  };
  if (compareFeature.angularityCat == control.getSelectedFeature().angularityCat){
    totalScore += parseFloat(angularRate);
  };

  return {
    relationImportance: relationRate,
    relations: relationsSimilarities,
    score: totalScore
  }
};

Query.prototype.compareDistance = function (distances1, distances2) {
  //improvisation
  var distance1 = Base.mean(distances1);
  var distance2 = Base.mean(distances2);

  return _.max([0, 1 - (Math.abs(distance1 - distance2))/300])
};

Query.prototype.compareTopology = function (topology1, topology2) {
  if (topology1 == topology2){
    return 1
  }else {
    return 0
  }
};

Query.prototype.compareAngle = function (angle1, angle2, lvl) {
  //also just an improvisation
  if (lvl == 1){
    var angle1 = Base.mean(angle1);
    var angle2 = Base.mean(angle2);

    var angleDiff = Math.abs(angle1 - angle2);

    if (angleDiff > 360){
      angleDiff = angleDiff - 360
    }
    return  Math.abs(1 - (angleDiff / 180)) ;
  }
  else{
    if (angle1 === angle2){
      return 1;
    }else{
      return 0;
    }
  }
};

Query.prototype.nextActiveResult = function (activeResultFeatureId){
  var activeIndex = this.indexOfResults(activeResultFeatureId);

  if (activeIndex > -1){
    if (this.resultFeatures[activeIndex + 1]){
      return this.resultFeatures[activeIndex + 1];
    }else{
      return _.find(this.resultFeatures, {id: activeResultFeatureId});
    };
  }else{
    return false
  };
};

Query.prototype.previousActiveResult = function (activeResultFeatureId){
  var activeIndex = this.indexOfResults(activeResultFeatureId);

  if (activeIndex > -1){
    if (this.resultFeatures[activeIndex - 1]){
      return this.resultFeatures[activeIndex - 1];
    }else{
      return _.find(this.resultFeatures, {id: activeResultFeatureId});
    };
  }else{
    return false;
  };
};

Query.prototype.indexOfResults = function (featureId){
  var feature = _.find(this.resultFeatures, {id: featureId});

  if (feature){
    return _.indexOf(this.resultFeatures, feature);
  }else{
    return -1;
  };
};

Query.prototype.setResultFeatures = function (resultFeatures){
  this.resultFeatures = resultFeatures;
};

Query.prototype.getResultFeatureById = function (featureId){
  return _.find(this.resultFeatures, {id: featureId});
};
