var Questionnaire = function(){
  this.html = '';
  this.open = true;
  this.div = $('#questionnaire-wrapper');
  this.refresh();
};

Questionnaire.prototype.refresh = function () {
  if (this.open){
    this.div[0].innerHTML = this.createHtml();
    this.setListeners();
    $('#questionnaire').modal();
  }else{
    this.div[0].innerHTML = '';
    $('.modal-backdrop').remove();
    $('#questionnaire').modal('hide');
  };
};

Questionnaire.prototype.toggleOpen = function () {
  this.open = $('#questionnaire').hasClass('in');
  this.open = !this.open;
  this.refresh();
};

Questionnaire.prototype.createHtml = function () {
  var html = '<div class="modal" id="questionnaire" data-backdrop="static">';
  html += '<div class="modal-dialog">';
  html += '<div class="modal-content">';
  html += '<div class="modal-header">';
  html += '<h4 class="modal-title">' + translate('Questionnaire') + '</h4>';
  html += '</div>';
  html += '<div class="modal-body">';

  html += '<dl class="dl-horizontal">';

  // age
  html += '<p><dt>';
  html += translate('age') + ': </dt><dd><select id="user-age-select" value="' + user.age + '">';
  html += '<option value="0-12">0 - 12</option>';
  html += '<option value="13-18">13 - 18</option>';
  html += '<option value="19-30">19 - 30</option>';
  html += '<option value="31-50">31 - 50</option>';
  html += '<option value="51+">51+</option>';
  html += '</select>';
  html += '</dd></p>';

  // gender
  html += '<p><dt>';
  html += translate('gender') + ': </dt><dd><select id="user-gender-select" value="' + user.gender + '">';
  html += '<option value="m">' + translate('man') + '</option>';
  html += '<option value="w">' + translate('woman') + '</option>';
  html += '</select>';
  html += '</dd></p>';

  // field of study
  html += '<p><dt>';
  html += translate('field') + ': </dt><dd><select id="user-field-select" value="' + user.age + '">';
  html += '<option value="geosciences - natural sciences path">' + translate('geosciences - natural sciences path') + '</option>';
  html += '<option value="geosciences - social path">' + translate('geosciences - social path') + '</option>';
  html += '<option value="geosciences - technical path">' + translate('geosciences - technical path') + '</option>';
  html += '<option value="technical">' + translate('technical') + '</option>';
  html += '<option value="not technical">' + translate('not technical') + '</option>';
  html += '<option value="other">' + translate('other') + '</option>';
  html += '</select>';
  html += '</dd></p>';

  // maps - topographic
  html += '<p><dt>';
  html += translate('topographic maps use') + ': </dt><dd><select id="user-maps-topographic-select" value="' + user.age + '">';
  html += '<option value="rarely">' + translate('rarely') + '</option>';
  html += '<option value="sometimes">' + translate('sometimes') + '</option>';
  html += '<option value="often">' + translate('often') + '</option>';
  html += '</select>';
  html += '</dd></p>';

  // maps - thematic
  html += '<p><dt>';
  html += translate('thematic maps use') + ': </dt><dd><select id="user-maps-thematic-select" value="' + user.age + '">';
  html += '<option value="rarely">' + translate('rarely') + '</option>';
  html += '<option value="sometimes">' + translate('sometimes') + '</option>';
  html += '<option value="often">' + translate('often') + '</option>';
  html += '</select>';
  html += '</dd></p>';

  html += '</dl>';

  html += '</div>';
  html += '<div class="modal-footer">';
  html += '<button type="button" class="save-changes btn btn-primary">' + translate('Save and continue') + '</button>';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  return html;
};

Questionnaire.prototype.setListeners = function () {
  var that = this;
  $('#user-age-select').val(user.age);
  $('#user-field-select').val(user.field);
  $('#user-maps-thematic-select').val(user.maps.thematic);
  $('#user-maps-topographic-select').val(user.maps.topographic);
  $('#user-gender-select').val(user.gender);

  $('#questionnaire .save-changes').click(function(){
    user.age = $('#user-age-select').val();
    user.field = $('#user-field-select').val();
    user.gender = $('#user-gender-select').val();
    user.maps.thematic = $('#user-maps-thematic-select').val();
    user.maps.topographic = $('#user-maps-topographic-select').val();
    if (!user.age || !user.field || !user.gender || !user.maps.thematic || !user.maps.topographic){
      that.toggleOpen();
      control.showAlert(translate('please fill in all data!'), '');
      setTimeout(function(){
        control.hideAlert();
        that.toggleOpen();
      }, 1000)
    }else{
      that.toggleOpen();
    }
  });
};
