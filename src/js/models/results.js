var Results = function (queryO) {
  this.html = '';
  this.query = queryO;
  this.activeResult = 0;
  this.rating = 4;
  this.rated = false;
};

Results.prototype.createHtml = function () {
  var html = '<div class="panels"  id="result-panel">';

  html += this.renderSizeAndAngularity();

  html += '<p><i class="search-for-results btn btn-danger glyphicon glyphicon-search">  ' + translate('update results') + '</i></p>';
  html += this.renderSimilarObject();
  html += '</div>';

  return html;
};

Results.prototype.renderSizeAndAngularity = function () {
  var feat = control.getSelectedFeature();

  var html = '';
  if (feat) {
    if (tm.typeOfLayer(feat) != 'Point'){
      // size
      html += '<h4>' + translate('size') + '</h4>';
      html += translate('size') + ': <str>' + translate(SIZECATEGORIES[feat.sizeCat]) + '</str>';

      html += '<div class="slider relation-rate-slider-div">';
      html += translate('importance');
      html += ': ' + Base.toPercentual(this.query.size);
      html += '<input class="size-rate-slider relation-rate-slider" type="range" min="0" max="0.3" value="' + this.query.size + '" step="0.05" />';
      html += '</div>';
      html += '</br>';
      html += '</br>';

      // angularity
      html += '<h4>' + translate('angularity') + '</h4>';
      html += translate('angularity') + ': <str>' + translate(ANGULARITYCATEGORIES[feat.angularityCat]) + '</str>';

      html += '<div class="slider relation-rate-slider-div">';
      html += translate('importance');
      html += ': ' + Base.toPercentual(this.query.angularity);
      html += '<input class="angularity-rate-slider relation-rate-slider" type="range" min="0" max="0.3" value="' + this.query.angularity + '" step="0.05" />';
      html += '</div>';
      html += '</br>';
      html += '</br>';
    }else{
      this.query.angularity = 0;
      this.query.size = 0;
    }
  }

  return html;
};

Results.prototype.refresh = function () {
  panel.refresh();
  this.setListeners();
};

Results.prototype.renderRating = function () {
  var html = '<div class="panel panel-info">';
  html += '<div class="panel-heading result-panel-rating">';
  html += '<h5 class="panel-title">' + translate('rating') + '</h5>'
  html += '</div>';
  html += '<div class="panel-body result-panel-body">';

  html += '<p><span class="btn-group pull-left">';

  for (var i = 1; i < 6; i++){
    if (this.rating >= i){
      html += '<i rating=' + i + ' class="result-rating btn btn-info glyphicon glyphicon-star btn-xs"> </i>';
    }else{
      html += '<i rating=' + i + ' class="result-rating btn btn-default glyphicon glyphicon-star-empty btn-xs"> </i>';
    }
  }

  html += '</span>';
  html += '<span class="btn-group pull-right"><i id="send-rating" class="btn btn-info glyphicon glyphicon-flash btn-xs';

  if (this.rated){
    html += ' disabled ';
  };

  html += '">' + translate('send') + '</i></span></p>';
  html += '</div>';
  html += '</div>';

  return html;
}

Results.prototype.renderSimilarObject = function () {
  var that = this;
  var html = '';
  var activeResultIndex = control.indexOfActiveResult();
  var activeResultFeature = control.activeResult();
  var selectedFeature = control.getSelectedFeature();

  if (activeResultFeature){
    html = '<div class="panel panel-primary">';

    //header
    html += '<div class="panel-heading result-panel-heading">';
    html += '<span class="btn-group pull-left">';
    html += '<h5 class="panel-title">';

    html += (activeResultIndex + 1) + '/' + this.query.resultFeatures.length;
    html += ', ' + translate('rating') + ': ' + Base.toPercentual(activeResultFeature.score);
    html += '</h5>';

    html += '</span>';

    html += '<span class="btn-group pull-right">';
    html += '<i id="previous-result-button" class="btn btn-default glyphicon glyphicon-menu-left btn-xs"> </i>';
    html += '<i id="next-result-button" class="btn btn-default glyphicon glyphicon-menu-right btn-xs"> </i>';
    html += '</span>';

    html += '</div>';

    //body
    html += '<div class="panel-body result-panel-body">';

    html += '<h4>ID: ' + control.activeResult().id + '</h4>';
    html += this.renderRating();


    if (tm.typeOfLayer(activeResultFeature) != 'Point') {
      html += '<dl class="dl-horizontal">';
        html += '<dt><strong>' + translate('size') + '</strong></dt>'
        html += '<dd><strong>' + translate(SIZECATEGORIES[activeResultFeature.sizeCat]) + '</strong>';
        html += '(';

        if (selectedFeature.sizeCat == activeResultFeature.sizeCat){
          html += '+' + Base.toPercentual(that.query.size);
        }else{
          html += '-' + Base.toPercentual(that.query.size);
        };
        html += ')</dd>';

        html += '<dt><strong>' + translate('angularity') + '</strong></dt>';
        html += '<dd><strong>' + translate(ANGULARITYCATEGORIES[activeResultFeature.angularityCat]) + '</strong>';
        html += '(';

        if (selectedFeature.angularityCat == activeResultFeature.angularityCat){
          html += '+' + Base.toPercentual(that.query.angularity);
        }else{
          html += '-' + Base.toPercentual(that.query.angularity);
        };
        html += ')</dd>';

      html += '</dl>';
    }else{
      html += '<strong>' + translate('not found') + '</strong>';
    }

    // relations
    html += '<h4>' + translate('relations') + ' <small>(' + translate('importance') + ': ' + Base.toPercentual(activeResultFeature.relationImportance) + ')</small></h4>';
    _.forEach (activeResultFeature.relations, function(relation, rid){
      html += that.renderRelation(activeResultFeature, relation, rid);
    })

    html += '</div>';
    html += '</div>';
  }
  return html
}

Results.prototype.renderRelation = function (activeResultFeature, relation, rid) {
  var html = '<div class="panel result-relation panel-default" rel_id="' + rid + '">';

    //header
    html += '<div class="panel-heading result-panel-heading">';
      html += '<span class="btn-group pull-left">';
        html += '<h5 class="panel-title">' + translate(relation.type)
        html += ' <small>(' + translate('importance') + ' '  + Base.toPercentual(relation.layerRate) + ')</small></h5>'
      html += '</span>';
    html += '</div>';

    //body
    html += '<div class="panel-body result-panel-body">';
      html += '<dl class="dl-horizontal">';

        if (relation.found){
          html += '<dt><strong>' + translate('total') + '</strong></dt><dd><strong>' +  Base.toPercentual(relation.score) + '</strong></dd>';
          html += '<dt>' + translate('distance') + '</dt><dd>' +  Base.toPercentual(relation.distance) + ' <br/><small>(' + translate('importance') + ' ' + Base.toPercentual(relation.distanceRate) + ')</small></dd>';
          html += '<dt>' + translate('angle') + '</dt><dd>' +  Base.toPercentual(relation.angle) + ' <br/><small>(' + translate('importance') + ' '  + Base.toPercentual(relation.angleRate) + ')</small></dd>';
          html += '<dt>' + translate('topology') + '</dt><dd>' +  Base.toPercentual(relation.topology) + ' <br/><small>(' + translate('importance') + ' '  + Base.toPercentual(relation.topologyRate) + ')</small></dd>';
        }else{
          html += '<strong>' + translate('not found') + '</strong>';
        }

      html += '</dl>';
    html += '</div>';

  html += '</div>';

  return html
}

Results.prototype.setListeners = function () {
  var that = this;

  $('.search-for-results').click(function(){
    control.findSimilarObjects();
  });

  $('#previous-result-button').click(function(){
    control.previousActiveResult();
    that.rated = false;
  });

  $('#next-result-button').click(function(){
    control.nextActiveResult();
    that.rated = false;
  });

  $('.result-relation').mouseenter(function(){
    var relId = $(this).attr('rel_id');
    if (relId != 'selected'){
      control.showHighlighter('result', relId);
    }
  });

  $('.result-relation').mouseleave(function(){
    control.hideHighlighter('result');
  });

  $('.result-rating').click(function(){
    that.changeRating($(this).attr('rating'));
  });

  $('.angularity-rate-slider').change(function(){
    that.query.angularity = $(this).val();
    panel.refresh();
  });

  $('.size-rate-slider').change(function(){
    that.query.size = $(this).val();
    panel.refresh();
  });

  // sending request to server to rate this similarity by user
  // rateObj will be saved to db
  $('#send-rating').click(function(){
    var rateObj = control.prepareRateObject();

    if (!that.rated){
      $.ajax({
        url: 'http://188.166.44.50:3000/rate',
        data: {'rate': rateObj},
        method: 'POST',
        success: function(data){
          control.showAlert(translate('rating saved'), translate('thank you for your rating!'));
          setTimeout( function () {
            that.rated = true;
            panel.refresh();
            control.hideAlert()
          }, 1000)
        }
      })
    }
  })
};

Results.prototype.changeRating = function (newRating) {
  this.rating = parseInt(newRating);
  control.refresh();
};
