var Selector = function Selector(id) {
  this.active = false;
  this.noBounds = [[1,1],[1,1]];
  this.bounds = [[1,1],[1,1]];
  this.style = {color: "#ff7800", weight: 1};
  this.geometry = new L.rectangle(this.bounds, this.style);
};

Selector.prototype.init = function (map) {
  this.geometry.addTo(map);
};

Selector.prototype.refresh = function () {
  this.geometry.setBounds(this.bounds);
  this.geometry.redraw();
};

Selector.prototype.changeBounds = function (bounds) {
  this.bounds = bounds;
  this.refresh();
};

Selector.prototype.remove = function () {
  this.changeBounds(this.noBounds);
};
