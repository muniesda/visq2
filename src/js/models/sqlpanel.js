var SqlPanel = function(){
  this.div = document.getElementById('sqlPanel');
  this.sql = '';
  this.closedHeight = 30;
  this.openHeight = 30;
};

SqlPanel.prototype.change = function(){
  this.sql = 'SELECT * FROM features AS d1 WHERE ';
  this.sql += this.selected();
  this.sql += this.relations();

  this.div.innerHTML = this.sql + '<br/>';
  this.openHeight = $(this.div).height();
  this.setListeners();
};

SqlPanel.prototype.selected = function(){
  var selectedTtype = control.getSelectedFeature()['typ'];
  var sql = 'd1.type IS "' + translate(query.selected['type']) + '" ';

  var attNames = _.keys(query.selected.filter);

  for (ani in attNames){
    if (query.selected.filter[attNames[ani]].values.length){
      var attributeId = attNames[ani];
      var attName = layers[selectedTtype]['attributes-helper'][attributeId][language];

      if(layers[selectedTtype]['attributes'][attributeId]['type'] == 'text'){
        sql += 'AND d1."' + attName + '" NOT IN ';
        sql += '(' + query.selected.filter[attributeId].values.join(', ') + ') ';
      }
      if(layers[selectedTtype]['attributes'][attributeId]['type'] == 'number'){
        if (query.selected.filter[attributeId].values[0]){
          sql += 'AND d1."' + attName + ' > ' + query.selected.filter[attributeId].values[0] + ' ';
        }
        if (query.selected.filter[attributeId].values[1]){
          sql += 'AND d1."' + attName + '" < ' + query.selected.filter[attributeId].values[1] + ' ';
        }
      }
    }
  };

  return sql
};

SqlPanel.prototype.relations = function(){
  var sql = '';
  var relations  = query.getActiveRelations();

  for (var ri in relations){
    var d = 'd' + (parseInt(ri) + 2);
    var relation = relations[ri];

    //console.log(relation);
    var relationIds = relation.features;
    sql += '<p> AND EXISTS( SELECT * FROM features AS ' + d + ' WHERE ';

    // type
    sql += d + '.type IS "' + relation.type + '" ';

    // distance, angular and topological conditions
    sql += 'AND ' + this.distance(relation, 'd1', d) + ' AND ' + this.notEqualIds('d1', d);
    sql += 'AND ' + this.angle(relation, 'd1', d) + ' AND ' + this.notEqualIds('d1', d);
    sql += 'AND ' + this.topology(relation.topology.sql, 'd1', d) + ' AND ' + this.notEqualIds('d1', d);

    sql += ') </p>';
  }

  return sql;
};

SqlPanel.prototype.notEqualIds = function(f1, f2){
  return f1 + '.id <> ' + f2 + '.id ';
};

SqlPanel.prototype.distance = function(relation, f1, f2){
  return 'ST_Distance(' + f1 + '.geometry, ' + f2 + '.geometry) = "' + relationsHelper.getDistance(relation) + '"';
};

SqlPanel.prototype.angle = function(relation, f1, f2){
  return 'degrees(ST_Azimuth(' + f1 + '.geometry, ' + f2 + '.geometry)) = "' + relationsHelper.getAngle(relation) + '"';
};

SqlPanel.prototype.topology = function(sqlQuery, f1, f2){
  return sqlQuery(f1 + '.geometry', f2 + '.geometry');
};

SqlPanel.prototype.setListeners = function(){
  var that = this;
  that.div.style.height = that.closedHeight + 'px';

  $('#sqlPanel').mouseover(function(){
    that.div.style.height = that.openHeight + 'px';
  });

  $('#sqlPanel').mouseout(function(){
    that.div.style.height = that.closedHeight + 'px';
  })
};
