/*
  tm is just a wrapper for library jsts. It contains functions for reading leaflet layers and features into native geometry format.
*/

var geoReader = new jsts.io.GeoJSONReader();
  var geoWriter = new jsts.io.GeoJSONWriter();
var ptDist = new jsts.algorithm.distance.PointPairDistance();
var hDist = new jsts.algorithm.distance.DiscreteHausdorffDistance();
var dist = new jsts.operation.distance.DistanceOp();
var angleAlg = new jsts.algorithm.Angle();
var noSurroundingRays = 16; // number of regular rays to send from feature to examining the surrounding

var tm = {
  geometryFactory: new jsts.geom.GeometryFactory(),
  read: function (obj) {
    if (obj.factory) { // to be refactored (some geometries do not need to have factory)
      return obj
    }else if(obj.geometry) {
      return geoReader.read(obj.geometry);
    }else{
      return geoReader.read(obj.toGeoJSON().geometry);
    }
  },
  write: function (geom) {
    return geoWriter.write(geom);
  },

  area: function (feat) {
    return (tm.read(feat).getArea() * TOMETERS * TOMETERS)/1000;
  },
  contains: function(feat1, feat2) {
    return tm.read(feat1).contains(tm.read(feat2));
  },
  covers: function(feat1, feat2) {
    return tm.read(feat1).covers(tm.read(feat2))
  },
  crosses: function(feat1, feat2) {
    return tm.read(feat1).crosses(tm.read(feat2))
  },
  disjoint: function (feat1,feat2) {
    return tm.read(feat1).disjoint(tm.read(feat2))
  },
  envelope: function (feat) {
    return tm.read(feat).getEnvelope();
  },
  equals: function (feat1, feat2) {
    return tm.read(feat1).equalsExact(tm.read(feat2), 0.001)
  },
  notEquals: function (feat1,feat2) {
    return !tm.equals(feat1,feat2);
  },
  intersects: function (feat1, feat2) {
    return tm.read(feat1).intersects(tm.read(feat2))
  },
  notIntersects: function (feat1, feat2) {
    return !tm.intersects (feat1, feat2);
  },
  intersection: function (feat1, feat2) {
    try {
      return tm.read(feat1).intersection(tm.read(feat2))
    }catch(err) {
      return false
    }
  },
  length: function (feat) {
    return 1000 * tm.read(feat).getLength();
  },
  coordToPoint: function (coord){
    var newPoint = new jsts.geom.Point(coord);
    newPoint.factory = tm.geometryFactory; // this is just a way to say, it is a geometry
    return newPoint;
  },
  linesConnect: function (line1, line2) {
    var lineCoords1 = tm.read(line1).getCoordinates();
    var lineCoords2 = tm.read(line2).getCoordinates();

    var l11 = tm.coordToPoint(lineCoords1[0])
    var l12 = tm.coordToPoint(lineCoords1.slice(-1)[0])
    var l21 = tm.coordToPoint(lineCoords2[0])
    var l22 = tm.coordToPoint(lineCoords2.slice(-1)[0])

    return tm.equals(l11, l21) || tm.equals(l11, l22) || tm.equals(l12, l21) || tm.equals(l12, l22)
  },
  linesJoint: function (line1, line2) {
    var lineIntersection = tm.intersection(line1, line2);
    if (lineIntersection){
      return tm.isLineType(tm.write(lineIntersection));
    };
    return false;
  },
  lineIntersects: function(line1, line2){
    return tm.intersects(line1, line2) || tm.crosses(line1, line2) || tm.touches(line1, line2)
  },
  lineEndsInPolygon: function(line, polygon){
    var lineCoords = tm.read(line).getCoordinates();
    return tm.inside(polygon, tm.coordToPoint(lineCoords[0])) || tm.inside(polygon, tm.coordToPoint(lineCoords.slice(-1)[0]))
  },
  overlaps: function (feat1, feat2) {
    return tm.read(feat1).overlaps(tm.read(feat2));
  },
  // is the point equal to one of ends of the line?
  pointEndsOfLine: function(point, line){
    var lineCoords = tm.read(line).getCoordinates();
    return tm.touches(tm.coordToPoint(lineCoords[0]), point) || tm.touches(tm.coordToPoint(lineCoords.slice(-1)[0]), point)
  },
  touches: function (feat1, feat2) {
    return tm.read(feat1).touches(tm.read(feat2)) || tm.read(feat2).touches(tm.read(feat1));
  },
  notTouches: function (feat1, feat2) {
    return !tm.touches(feat1, feat2);
  },

  within: function (feat1, feat2) {
    return tm.read(feat1).within(tm.read(feat2));
  },

  isWithinDistance: function (feat1, feat2, dist) {
    return tm.read(feat1).isWithinDistance(tm.read(feat2), 100)
  },

  polygonInteraction: function (feat1, feat2) {
    return tm.touches(feat1, feat2) || tm.intersects(feat1, feat2) || tm.inside(feat1, feat2, false)
  },
  polygonNotInteraction: function (feat1, feat2) {
    return !tm.polygonInteraction(feat1, feat2)
  },
  angleChangeRotation: function (degrees) {
    return (-1 *(degrees - 360)) - 90;
  },
  angleRadsToDegrees: function (rads) {
    return (rads * 180)/ Math.PI
  },

  buffer: function (feat,dist, changeLL) {
    var geom = tm.read(feat);
    var bufferOp = new jsts.operation.buffer.BufferOp(geom,null);
    var buffer = bufferOp.getResultGeometry(dist/TOMETERS);
    wBuffer =  tm.write(buffer);

    //change lat and long
    if (changeLL){
      for (var ci in wBuffer.coordinates[0]){
        var l1 = wBuffer.coordinates[0][ci][0];
        var l2 = wBuffer.coordinates[0][ci][1];
        wBuffer.coordinates[0][ci][0] = l2;
        wBuffer.coordinates[0][ci][1] = l1;
      }
    }

    return wBuffer
  },
  ptDist: function (point1, point2) {
    var dist = new jsts.algorithm.distance.PointPairDistance();
    dist.initialize(tm.read(point1).coordinate, tm.read(point2).coordinate)
    return dist.distance*TOMETERS;
  },
  toPtDist: function (geom, point) {
    var dist = new jsts.algorithm.distance.DistanceToPoint();
    var ptDist = new jsts.algorithm.distance.PointPairDistance();
    //ptDist.initialize(tm.read(geom).coordinate, tm.read(point).coordinate);
    return dist.minimum*TOMETERS;
  },
  hausdorff: function (feat1, feat2) {
    var geom1 = tm.read(feat1);
    var geom2 = tm.read(feat2);
    // if(geom1.getGeometryType() == "Point" || geom2.getGeometryType() == "Point"){
    //   jsts.algorithm.distance.DistanceToPoint.computeDistance(geom1, geom2.getCoordinate(), ptDist);
    //   console.log (ptDist.getDistance())
    //   return jsts.algorithm.distance.DistanceToPoint(geom1, geom2)
    // }else{
    var hDist = new jsts.algorithm.distance.DiscreteHausdorffDistance(geom1.geometries[0], geom2);
    var dist = hDist.computeDistance(ptDist);
    return dist.getDistance();
    // }
  },

  manhattan: function (feat1, feat2) {
    var dist = new jsts.operation.distance.DistanceOp(tm.read(feat1), tm.read(feat2));

    if (dist.distance()) {
      var point1 = dist.nearestPoints()[0];
      var point2 = dist.nearestPoints()[1];
      var point3 = point1.clone();
      point3.y = point2.y;

      var distance = point1.distance(point3) + point2.distance(point3);
      return distance*TOMETERS
    }else{
      return null
    }
  },

  numberOfFeaturesInside: function (layers, filter, what){ // filter is the name of layer we want to calculate with. False means all layers
    var inside = [];

    for (var l in layers) {
      var layer = layers[l];

      if (layer.name == filter || filter == false){
        for (var f in layer._layers) {
          var feat = layer._layers[f];
          if (tm.inside(what, feat, false) ){
            inside.push(feat);
          }
        }
      }
    }

    return inside.length
  },

  //Distance : line/polygon and point
  minDistanceOfPathAndPoint: function (l,p) {
    var minD = Number.MAX_VALUE;
    var lls = l.getLatLngs();
    if (lls.length == 1) {
      lls = lls[0];
    }
    for (var v in lls) {
      var d = p.distanceTo(lls[v]);
      if (d < minD) {
        minD = d;
      }
    }
    return minD;
  },

  inside: function (feat1, feat2, fully) {
    if (fully) {
      return tm.contains(feat1, feat2);
    }else{
      if (tm.intersects(feat1, feat2) || tm.overlaps(feat1, feat2)) {
        return true
        if (tm.distance(feat1, feat2) == 0) {
        };
      };
      return false
    };
  },

  polygonInsidePolygon: function(pol1, pol2){
    return tm.inside(pol1, pol2, true) || tm.inside(pol2, pol1, true);
  },

  outside: function (feat1, feat2) {
    return !tm.inside(feat1, feat2, true);
  },

  //check if there is even a single feature inside rectangle
  isLayerInsideRectangle: function (rect, layer, fully) {
    var is = false;
    //console.log(layer);
    for (var l in layer._layers) {
      if (tm.inside(rect, layer._layers[l], fully)) {
        is = true;
        break;
      }
    }
    //console.log(is);
    return is;
  },

  //returns layers inside rectangle
  layersInsideRectangle: function (rect, layers, fully) {
    inside = [];
    for (var i in layers) {
      if (tm.isLayerInsideRectangle(rect, layers[i], fully)) {
        inside.push(layers[i]);
      }
    }
    return inside;
  },


  minDistanceOfLayerFromPoint: function (layer, feat) {
    var minD = Number.MAX_VALUE;
    for (var l in layer._layers) {
      var distance;
      if (tm.typeOfLayer(layer) == 'Polygon') {
        distance = tm.distance(tm.centroid(layer._layers[l]),center)
      }else{
        distance = tm.distance(layer._layers[l],center)
      }
      if (distance < minD) {
        minD = distance;
      }
    }
    return minD;
  },

  //
  findClosestLayerToCenter: function (layers, feat) {
    var closest;
    var minD = Number.MAX_VALUE;

    for (var i in layers) {
      var distance = tm.minDistanceOfLayerFromPoint(layers[i],feat);
      //console.log ('layer: ' + layers[i].name + ' , distance: ' + distance);
      if (distance < minD) {
        minD = distance;
        closest = layers[i];
      }
    }
    return closest;
  },

  //
  //RECTANGLE
  //
  //returns central coordinates of rectangle
  centerOfRectangle: function (rect) {
    return rect.getBounds().getCenter();
  },
  //returns array with bounds coordinates of rectangle
  sOfRect: function (rect) {
    return rect.getBounds().getSouth();
  },
  nOfRect: function (rect) {
    return rect.getBounds().getNorth();
  },
  eOfRect: function (rect) {
    return rect.getBounds().getEast();
  },
  wOfRect: function (rect) {
    return rect.getBounds().getWest();
  },
  sEofRect: function (rect) {
    return rect.getBounds().getSouthEast();
  },
  sWofRect: function (rect) {
    return rect.getBounds().getSouthWest();
  },
  nEofRect: function (rect) {
    return rect.getBounds().getNorthEast();
  },
  nWofRect: function (rect) {
    return rect.getBounds().getNorthWest();
  },
  widthOfRect: function (rect) {
    return tm.eOfRect(rect) - tm.wOfRect(rect);
  },
  heightOfRect: function (rect) {
    return tm.nOfRect(rect) - tm.sOfRect(rect);
  },

  //returns number of features inside rectangle.
  numberOfFeaturesInsideRectangle: function (rect, layer, fully) {
    var no = 0;
    for (var l in layer._layers) {
      if (tm.inside(rect, layer._layers[l], fully)) {
        no++;
      }
    };
    return no;
  },

  //returns layer with most features inside rectangle
  maxNumberOfFeaturesInsideRectangle: function (rect, layers, fully) {
    var maxN = 0;
    var most;
    for (var i in layers) {
      var number = tm.numberOfFeaturesInsideRectangle(rect, layers[i], fully);
      if (number > maxN) {
        maxN = number;
        most = layers[i];
      }
    };
    return most;
  },

  size: function (feature) {
    var typeOfFeature = tm.typeOfLayer(feature);

    if (typeOfFeature == 'LineString'){
      return tm.length(feature)
    }
    else if (typeOfFeature == 'Polygon'){
      return tm.area(feature)
    };

    return 0
  },

  angularity: function (feature) {
    if (tm.typeOfLayer(feature) == 'Point'){
      return 0;
    };
    return tm.length(feature) / tm.length(tm.envelope(feature));
  },

  typeOfLayer: function (layer) {
    if (layer) {
      if (_.includes(['Point', 'LineString', 'Polygon', 'MultiLineString', 'GeometryCollection', 'MultiPoint'], layer.type)){
        return layer.type;
      }else if (layer.geometry) {
        return layer.geometry.type;
      }else if (!layer.feature) {
        console.log(layer);
        return layer.getLayers()[0].feature.geometry.type;
      }else{
        return layer.feature.geometry.type;
      }
    }
  },

  isPoint: function (feat) {
    return tm.typeOfLayer(feat) == 'Point';
  },
  isPolygon: function (feat) {
    return tm.typeOfLayer(feat) == 'Polygon';
  },
  isLine: function (feat) {
    return tm.typeOfLayer(feat) == 'LineString';
  },
  isLineType: function (feat) {
    return tm.typeOfLayer(feat) == 'LineString' || tm.typeOfLayer(feat) == 'Multiline' ;
  },

  densityInRect: function (rect, layer, fully) {
    var no = tm.numberOfFeaturesInsideRectangle(rect, layer, fully);
    var area = tm.areaOfRect(rect);
    return no/area;
  },

  sumOfAreas: function (rect, layer) {
    if (tm.typeOfLayer(layer.getLayers()[0]) == 'Polygon') {
      var inside = tm.featuresInsideRectangle(rect, layer);
      var area = 0;
      for (var i in inside) {
        //var layerC = tm.clipPolygonsByRect(rect, inside[i])
        area += tm.area(inside[i]);
      };
      return area;
     }else{
       return 0;
     }
  },

  featuresInsideRectangle: function (rect, layer) {
    var inside = [];
    for (var l in layer._layers) {
      if (tm.inside(rect, layer._layers[l], false)) {
        inside.push(layer._layers[l]);
      }
    };
    return inside;
  },

  /*
   returns [feature, distance]
  */
  closestFeatureToPoint: function (features, point) {
      var minD = Number.MAX_VALUE;
      var closestFeature;
      for (var fi in features) {
        var feature = features[fi];
        var centroid = tm.centroid(feature)
        var distance = tm.distance(centroid,point);
        if (distance < minD) {
          minD = distance;
          closestFeature = feature
        }
      }
      return [closestFeature, minD];
  },

  mouseselectFeature: function(features, cursor, maxDistance){
    var minD = maxDistance;
    var closestFeature;
    for (var fi in features) {
      var feature = features[fi];
      var distance;

      if (tm.isLineType(feature) || tm.isPoint(feature)){
        distance = tm.distance(feature,cursor);
      }else {
        var centroid = tm.centroid(feature);
        distance = tm.distance(centroid,cursor);
      };
      if (distance && distance < minD) {
        minD = distance;
        closestFeature = feature
      }
    }
    return [closestFeature, minD];
  },

  /*
   returns [feature, distance]
  */
  closestFeatureToRectangle: function (features, rect) {
    return tm.closestFeatureToFeature(features, tm.centroid(rect))
  },


  /*
   returns [feature, distance]
  */
  closestFeatureToFeature: function (features, rfeature) {
    var minD = Number.MAX_VALUE;
    var closestFeature;
    for (var fi in features) {
      var feature = features[fi];
      var distance = tm.distance(feature,rfeature);
      if (distance < minD) {
        minD = distance;
        closestFeature = feature
      }
    }
    return [closestFeature, minD];
  },

  /*
   returns features []
  */
  findFeatureInRadius: function(features, rfeature, radius){
    var inRadius = [];

    for (var fi in features) {
      var feature = features[fi];

      var distance = tm.distance(feature,rfeature);
      if (distance < radius && feature.id != rfeature.id) {
        inRadius.push({'feature': feature, 'distance': distance})
      }
    }
    var inRadiusSorted = _.sortBy(inRadius, 'distance')

    return _.pluck(inRadiusSorted, 'feature');
  },


  /*
   returns [averageDistance number, distances []]
  */
  averageDistanceForGroup: function(features, rfeature){
    var distances = [];

    for (var fi in features) {
      var feature = features[fi];
      distances.push(tm.distance(feature,rfeature))
    }

    return [_.sum(distances)/distances.length, distances];
  },


  /*
  CALCULATING ANGLES
  */
  //returns angle number
  angle: function (rfeat, feats) {
    var from = tm.referencePoint(rfeat);

    var that = tm;

    var singleAngle = function(f){
      var to = that.referencePoint(f);

      var intersection = that.intersection(rfeat, f);

      if (intersection){
        if (!intersection.isEmpty()){
          to = that.centroid(intersection, false);
        }
      };
      return that.anglePointToPoint(from, to);
    }


    if (feats.length > 1){
      var angles = []
      _.forEach(feats, function(feat, fi){
        angles.push(singleAngle(feat))
      })
      return _.sum(angles)/angles.length
    }else{
      return singleAngle(feats)
    }

  },
  anglePointToPoint:  function (point1, point2) {
    var dx = point1.coordinate.x - point2.coordinate.x;
    var dy = point1.coordinate.y - point2.coordinate.y;
    var rads = Math.atan2(dy, dx);

    var degrees = tm.angleRadsToDegrees(rads);
    degrees = tm.angleChangeRotation(degrees);
    return tm.angleValidate(degrees);
  },
  angleToQuadrant: function (degrees) {
    return ANGLES.quadrant[Math.floor(tm.angleValidate(degrees+45)/90)];
  },
  angleToOctant: function (degrees) {
    return ANGLES.octant[Math.floor(tm.angleValidate(degrees+22.5)/45)];
  },
  angleToSemi: function (degrees) {
    return ANGLES.semi[Math.floor(tm.angleValidate(degrees+90)/180)];
  },
  angleValidate: function (degrees) {
    while (degrees > 360) {
      degrees -= 360
    }while (degrees < 0) {
      degrees += 360
    }
    return degrees;
  },

  /*
    returns the interval distance range values for given distance
  */
  generalizeAngle: function (angle, level) {
    if (level == 0){
      return angle
    }else if (level == 1){
      return [parseInt(angle - 10), parseInt(angle + 10)]

    }else if (level == 2){
      if (_.inRange(angle, 45, 135)){
        return 'quadrantE'
      }
      if (_.inRange(angle, 135, 225)){
        return 'quadrantS'
      }
      if (_.inRange(angle, 225, 315)){
        return 'quadrantW'
      }
      if (_.inRange(angle, 315, 360) || _.inRange(angle, 0, 45)){
        return 'quadrantN'
      }

    }else if (level == 3){
      if (_.inRange(angle, 90, 270)){
        return 'semiS'
      }
      if (_.inRange(angle, 0, 90) || _.inRange(angle, 270, 360)){
        return 'semiN'
      }
    }
  },

  // like centroid, but in case of line, it fits a vector
  // always returns leaflet point
  referencePoint: function (feat) {
    if (tm.isLine()){
      return tm.getMiddleCoordinate(feat);
    }else{
      return tm.centroid(feat, false);
    }
  },

  /*
    returns leaflet coordinates if second argument is true
  */
  centroid: function (feat,toLL) {
    var center;

    if (feat instanceof Array) {
      center = tm.read(feat[0]).getCentroid();

      var xs = [];
      var ys = [];
      for (var i in feat) {
        var f = feat[i];
        xs.push (tm.read(f).getCentroid().coordinate.x)
        ys.push (tm.read(f).getCentroid().coordinate.y)
      }
      center.coordinate.x = _.sum(xs)/xs.length;
      center.coordinate.y = _.sum(ys)/ys.length;
    }
    else{
      center = tm.read(feat).getCentroid()
    }
    if (toLL) {
      if (center.coordinate.y && center.coordinate.x) {
        return L.latLng(center.coordinate.y, center.coordinate.x);
      }else{
        return null
      }
    }

    return center
  },

  /*
    returns the middle vertex
  */
  getMiddleCoordinate: function (feat) {
    var coords = tm.read(feat).getCoordinates();
    var len = coords.length;

    if (len == 0){
      return false
    }else{
      return coords[Math.floor(len/2)]
    }
  },

  // secondary function for working with intervals
  // works only with natural numbers
  getInterval_: function (value, interval) {
    return [parseInt(value/interval) * interval, (parseInt(value/interval) + 1) * interval]
  },


  /*
  DISTANCE
  */
  distance: function (feat1,feat2) {
    if (_.isArray(feat1)){

    }
    dist = new jsts.operation.distance.DistanceOp(tm.read(feat1), tm.read(feat2));
    return dist.distance()*TOMETERS
  },


  /*
    returns the interval distance range values for given distance
  */
  generalizeDistance: function (distance, level) {
    if (level == 0){
      return distance
    }else if (level == 1){
      return tm.getInterval_(distance, 5)
    }else if (level == 2){
      return tm.getInterval_(distance, 20)
    }else if (level == 3){
      return tm.getInterval_(distance, 100)
    }
  },


  // SURROUNDINGS
  /*
    method will calculate if feature is surrounded
    rays - triangles representing circular sectors of radius around reference feature
    features - features around reference feature
    returns Boolean
  */
  isSurrounded: function (rays, features){
    var surrouindedCoefficients = tm.checkSurroundingInRays(rays, features);
    var totalC = surrouindedCoefficients[0] / noSurroundingRays;
    var inLineC = surrouindedCoefficients[1] / noSurroundingRays;

    return inLineC > SURROUNDINGCOEFFICIENTINLINE || totalC > SURROUNDINGCOEFFICIENTTOTAL;
  },

  /*
    returns array with total number of rays with intersection
      and maximum number of rays with intersection in-line
  */
  checkSurroundingInRays: function (rays, feats){

    // max number of intersected rays in line
    var inLine = [];
    var maxInLine = 0;
    var nowInLine = 0;

    // total number of intersected rays
    var total = 0;

    for (ri in rays){
      var intersecting = false;
      var ray = rays[ri];

      for (fi in feats){
        var feat = feats[fi];

        if (tm.polygonInteraction(ray, feat)){
          intersecting = true;
        }
      };

      if (intersecting){
        total = total + 1;

        nowInLine = nowInLine + 1;
        inLine.push(nowInLine);
      }else{
        nowInLine = 0
      };
    };

    var maxInLine = _.max(inLine);

    return [total, maxInLine];
  },

  /*
    creates circular sectors of given radius around given features
  */
  createRays: function (features, radius){
    var rays = [];
    var rCentroid = this.centroid(features).getCoordinates()[0];
    var rayDistance = radius/TOMETERS;

    var createRayVertex = function(pi){
      var diffX = Math.sin(pi/57.2958) * rayDistance;
      var diffY = Math.cos(pi/57.2958) * rayDistance;

      var newPoint = rCentroid.clone();
      newPoint.x += diffX;
      newPoint.y += diffY;

      return newPoint
    }

    for (var i=0; i < noSurroundingRays; i++){
      var pi1 = i*(1/(noSurroundingRays/2))*180;
      var pi2 = (i+1)*(1/(noSurroundingRays/2))*180;

      var raySegment = L.polygon([
        [rCentroid.y, rCentroid.x],
        [createRayVertex(pi1).y, createRayVertex(pi1).x],
        [createRayVertex(pi2).y, createRayVertex(pi2).x]
      ])

      // placing the segment on map - just for testing
      //raySegment.addTo(map2.map);

      rays.push(raySegment)
    }

    return rays;
  },

}
