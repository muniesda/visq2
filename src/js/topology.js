var topologyTypes = {
  'Point': {
    'Point': [
      {
        'degree1': 'identical',
        'confirm': tm.equals,
        'degree2': 'with interaction',
        'degree3': 'active',
        'sql': function(geom1, geom2){
          return 'ST_Equals(' + geom1 + ', ' + geom2 +') = false ';
        }
      },
      {
        'degree1': 'unrelated',
        'confirm': tm.notEquals,
        'degree2': 'without interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Equals(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
		],
    'LineString': [
      {
        'degree1': 'ending in',
        'confirm': tm.pointEndsOfLine,
        'degree2': 'with interaction',
        'degree3': 'active',
        'sql': function(geom1, geom2){
          return 'ST_Equals(ST_StartPoint(' + geom1 + '),'  + ',' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'touch',
        'confirm': tm.touches,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Touches(' + geom1 + ', ' + geom2 +') = false ';
        }
      },
      {
        'degree1': 'unrelated',
        'confirm': tm.notTouches,
        'degree2': 'without interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Touches(' + geom1 + ', ' + geom2 +') = false ';
        }
      },
  	],
    'Polygon': [
      {
        'degree1': 'touch',
        'confirm': tm.touches,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Touches(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'inside',
        'confirm': tm.inside,
        'degree2': 'with interaction',
        'degree3': 'active',
        'sql': function(geom1, geom2){
          return 'ST_Contains(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'outside',
        'confirm': tm.outside,
        'degree2': 'without interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Disjoint(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
  	],
  },

  'LineString': {
    'LineString': [
      {
        'degree1': 'joint(merged)',
        'confirm': tm.linesJoint,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Equals(' + geom1 + ', ' + geom2 +') = true '; // ??
        }
      },
      {
        'degree1': 'connected',
        'confirm': tm.linesConnect,
        'degree2': 'with interaction',
        'degree3': 'active',
        'sql': function(geom1, geom2){
          return 'ST_Equals(' + geom1 + ', ' + geom2 +') = true '; // ??
        }
      },
      {
        'degree1': 'touch',
        'confirm': tm.touches,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Touches(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'cross',
        'confirm': tm.lineIntersects,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Crosses(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'unrelated',
        'confirm': tm.notIntersects,
        'degree2': 'without interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Disjoint(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
  	],
    'Polygon': [
      {
        'degree1': 'ending in',
        'confirm': tm.lineEndsInPolygon,
        'degree2': 'with interaction',
        'degree3': 'active',
        'sql': function(geom1, geom2){
          return 'ST_Equals(ST_StartPoint(' + geom1 + '),'  + ',' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'touch',
        'confirm': tm.touches,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Touches(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'going through',
        'confirm': tm.polygonInteraction,
        'degree2': 'with interaction',
        'degree3': 'active',
        'sql': function(geom1, geom2){
          return 'ST_Equals(' + geom1 + ', ' + geom2 +') = true '; // ??
        }
      },
      {
        'degree1': 'unrelated',
        'confirm': tm.notIntersects,
        'degree2': 'without interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Disjoint(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
  	],
  },

  'Polygon': {
    'Polygon': [
      {
        'degree1': 'inside',
        'confirm': tm.polygonInsidePolygon,
        'degree2': 'with interaction',
        'degree3': 'active',
        'sql': function(geom1, geom2){
          return 'ST_Contains(' + geom1 + ', ' + geom2 +') OR ST_Contains(' + geom2 + ', ' + geom1 +') = true ';
        }
      },
      {
        'degree1': 'touch',
        'confirm': tm.touches,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Touches(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'intersection',
        'confirm': tm.intersects,
        'degree2': 'with interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Intersects(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
      {
        'degree1': 'unrelated',
        'confirm': tm.outside,
        'degree2': 'without interaction',
        'degree3': 'passive',
        'sql': function(geom1, geom2){
          return 'ST_Disjoint(' + geom1 + ', ' + geom2 +') = true ';
        }
      },
  	],
  },
}

var topoplogyHelper = function (group, features, selected) {
	featureType = tm.typeOfLayer(features[0]);
	selectedType = tm.typeOfLayer(selected);

	var topologies = []

	if ((featureType == 'Point') || (featureType == 'LineString' && selectedType != 'Point')){ // feature first
		topologies = topologyTypes[featureType][selectedType];
	}else{ //feature second
		topologies = topologyTypes[selectedType][featureType];
	};

  if (topologies){
		for (var ti in topologies) {
      var topology = topologies[ti];

      if (topology){
        if (topology['confirm']){
          for (var fi in features){
            var feature = features[fi];
            if (topology['confirm'](feature, selected)){
              return topology;
            }
          }
        }
      }
		}
  }

  return topologies[0]
}
